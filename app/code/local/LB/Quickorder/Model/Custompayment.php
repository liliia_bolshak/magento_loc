<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mypayment
 *
 * @author denis
 */

class LB_Quickorder_Model_Custompayment extends Mage_Payment_Model_Method_Abstract
{
    
    //unique internal payment method identifier
    protected $_code = 'custompayment';
    
    //flag if we need to run payment initialize while order place
    protected $_isInitializeNeeded      = true;
    
    //can use this payment method in administration panel?
    protected $_canUseInternal          = false;
    
    //can show this payment method as an option on checkout payment page?
    protected $_canUseCheckout          = false;
    
    //is this payment method suitable for multi-shipping checkout?
    protected $_canUseForMultishipping  = false;
    
    
   
    
    /**
     * Check whether payment method can be used
     */
    public function isAvailable()
    { 
        return true;
               
      
    }


}
