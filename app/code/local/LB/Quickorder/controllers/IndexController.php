<?php

class LB_Quickorder_IndexController extends Mage_Core_Controller_Front_Action
{
   
    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get current active quote instance
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        return $this->_getCart()->getQuote();
    }
    
    /**
     * create new order
     * 
     */
    public function addAction() {
      
        $post = $this->getRequest()->getPost();
        
        $shippingAddress = Mage::helper('quickorder')->checkCustomer($this->_getQuote(),$post);
        //set payment and sipping metod
        $shippingAddress->setCollectShippingRates(true)->collectShippingRates()
                        ->setShippingMethod('customshipping_customshipping')
                        ->setPaymentMethod('custompayment');

        $this->_getQuote()->getPayment()->importData(array('method' => 'custompayment'));
        $this->_getQuote()->collectTotals()->save();

        $service = Mage::getModel('sales/service_quote', $this->_getQuote());
        $service->submitAll();

        $orderId = $service->getOrder()->getId();
                 
        $this->_getSession()->setLastSuccessQuoteId($this->_getQuote()->getId())  
                            ->setLastQuoteId($this->_getQuote()->getId()) 
                            ->setLastOrderId($orderId);
        
        $this->_getQuote()->save();
        $this->_redirect('checkout/onepage/success');
     }   
     
     

}
