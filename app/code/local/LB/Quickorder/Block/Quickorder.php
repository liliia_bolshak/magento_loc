<?php


class LB_Quickorder_Block_Quickorder extends Mage_Checkout_Block_Cart_Abstract
{
    /**
     * get subtotal
     * @return type
     */
    public function getSubtotal()
    {
        $totals = $this->getQuote()->getBaseSubtotal();
    
        return $totals;
    }
    
    /**
     * 
     * @return form action
     */
    public function getFormAction()
    {
        return Mage::getBaseUrl(). 'quickorder/index/add' ;
    }
}
