<?php
class LB_Quickorder_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getUserName()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return '';
        }
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return trim($customer->getName());
    }

    public function getUserEmail()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return '';
        }
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return $customer->getEmail();
    }
    
    public function getUserPhone()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return '';
        }
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return $customer->getPrimaryBillingAddress()->getTelephone();
    }
    
    public function checkCustomer($quote,$post)
    {
        if(Mage::getSingleton('customer/session')->isLoggedIn()){
            
            $customer = Mage::getModel('customer/customer');
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail($post['email']);
            $quote->assignCustomer($customer);
            $shippingAddress = $quote->getShippingAddress();
            
            return $shippingAddress;       
        }else{
            
             $quote->setCustomerEmail($post['email']);
             $quote->setCustomerFirstname($post['name']);
             
             if($post['telephone'] == ''){
                $phone = 'Empty';           
             }else{
                $phone = $post['telephone'];
             }
             $customer = explode(' ',$post['name']);
             $customerFirstname = $customer[0];
                if($customer[1]){
                    $customerLastname = $customer[1];
                }else{
                    $customerLastname = '&nbsp;';
                }
                     
            $addressData = array(
            'firstname' => $customerFirstname,
            'lastname' => $customerLastname,
            'street' => 'Empty',
            'city' => 'Empty',
            'postcode' => 'Empty',
            'telephone' => $phone,
            'country_id'  => 'Empty'
            );
                
             $billingAddress = $quote->getBillingAddress()->addData($addressData);
             $shippingAddress = $quote->getShippingAddress()->addData($addressData); 
             return $shippingAddress;
        }

    }
}
