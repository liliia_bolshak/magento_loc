<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'citation/citation'
 */
$installer->getConnection()->dropTable('citation/citation');

$table = $installer->getConnection()
    ->newTable($installer->getTable('citation/citation'))
    
    ->addColumn('citation_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Citation id')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Product ID')
    ->addColumn('citation', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
        ), 'Citation')
    ->addColumn('date_publication', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => false,
    ), 'Date of publication')
    ->addColumn('source', Varien_Db_Ddl_Table::TYPE_CHAR, null, array(
        'nullable'  => false,
    ), 'Source')
    ->addColumn('link', Varien_Db_Ddl_Table::TYPE_CHAR, null, array(
        'nullable'  => false,    
    ), 'Reference to the source')
    ->addIndex($installer->getIdxName('citation/citation', array('product_id')),
        array('product_id'))
    ->addForeignKey($installer->getFkName('citation/citation', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Table citation products');
$installer->getConnection()->createTable($table);

/**
 * Create table 'citation/citations_store' for binding quotes to store
 */
$installer->getConnection()->dropTable('citation/citations_store');
$table = $installer->getConnection()
    ->newTable($installer->getTable('citation/citations_store'))
    ->addColumn('citation_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'primary'   => true,
        ), 'Citation ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')
    ->addIndex($installer->getIdxName('citation/citations_store', array('store_id')),
        array('store_id'))
    ->addForeignKey($installer->getFkName('citation/citations_store', 'citation_id', 'citation/citation', 'citation_id'),
        'citation_id', $installer->getTable('citation/citation'), 'citation_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('citation/citations_store', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Citations Store');
$installer->getConnection()->createTable($table);

$installer->endSetup();