<?php
class LB_Citation_Model_Resource_Citation extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('citation/citation', 'citation_id');
    }

}
