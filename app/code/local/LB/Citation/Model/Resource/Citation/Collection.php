<?php
class LB_Citation_Model_Resource_Citation_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Define resource model
     *
     */
    protected function _construct()
    {
        $this->_init('citation/citation');
    }
    
}
