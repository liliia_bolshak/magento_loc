<?php

class LB_Citation_Block_Adminhtml_Citation_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }
    
    protected function _prepareForm()
    {

        $model = Mage::registry('citation');

        /*
         * Checking if user have permissions to save information
         */
       
       
        if ($this->_isAllowedAction('save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }


        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('citation');

        $fieldset = $form->addFieldset('content_fieldset', array('legend'=>Mage::helper('citation')->__('Information'),'class'=>'fieldset-wide'));
        $this->_addElementTypes($fieldset);
        
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
            array('tab_id' => $this->getTabId())
        );
        if ($model->getCitationId()) {
            $fieldset->addField('citation_id', 'hidden', array(
                'name' => 'citation_id',
            ));
        }
  
        $helper = Mage::helper('citation');
        
        $productConfig = array(
                        'input_name' => 'entity_link',
                        'input_label' => $this->__('Product'),
                        'button_text' => $this->__('Select Product...'),
                        'required' => true
                        );
        $helper->createProductChooser($model, $fieldset, $productConfig); 

        $fieldset->addField('citation', 'editor', array(
            'name'      => 'citation',
            'label'     => $this->__('Citation'),
            'style'     => 'height:36em;',
            'required'  => true,
            'disabled'  => $isElementDisabled,
            'config'    => $wysiwygConfig
        ));
    
        
        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );
        $fieldset->addField('date_publication', 'date', array(
            'name'      => 'date_publication',
            'label'     => Mage::helper('stocks')->__('Date of publication'),
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'format'    => $dateFormatIso,
            'disabled'  => $isElementDisabled,
            'class'     => 'validate-date validate-date-range date-range-custom_theme-from'
        ));

        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('citation')->__('Store View'),
                'title'     => Mage::helper('citation')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
                'disabled'  => $isElementDisabled,
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }
        
        $fieldset->addField('source', 'text', array(
            'name'      => 'source',
            'label'     => Mage::helper('citation')->__('Source'),
            'title'     => Mage::helper('citation')->__('Source'),
            'required'  => true,
            'disabled'  => $isElementDisabled,
             'config'    => $wysiwygConfig
        ));


        Mage::dispatchEvent('adminhtml_news_edit_tab_content_prepare_form', array('form' => $form));

        $form->setValues($model->getData());
        $this->setForm($form);
       

        return parent::_prepareForm();
    }

    
       
    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('citation')->__('Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('citation')->__('Information');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('citation/' . $action);
    }
}
