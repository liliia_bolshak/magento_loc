<?php

class LB_Citation_Block_Adminhtml_Citation_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        
        parent::__construct();
        $this->setId('citationGrid');
        $this->_controller = 'adminhtml_citation';
        $this->setDefaultDir('ASC');
    }
    
    protected function _prepareCollection()
    {
        
        $collection = Mage::getModel('citation/citation')->getCollection();
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }
   
    protected function _prepareColumns()
    {
          
        $this->addColumn('citation_id', array(
            'header'        => Mage::helper('citation')->__('ID'),
            'index'         => 'citation_id',
            'filter_index'  => 'ct.citation_id',
            'width'         => '25px',
            'align'         => 'right',
        ));
        
        $this->addColumn('name', array(
            'header'    => Mage::helper('citation')->__('Product Name'),
            'align'     => 'left',
            'type'      => 'text',
            'index'     => 'name',
            'escape'    => true,
            'width'     => '200px'
        ));
        
        $this->addColumn('product_id', array(
            'header'        => Mage::helper('citation')->__('Product ID'),
            'index'         => 'product_id',
            'filter_index'  => 'ct.product_id',
            'width'         => '50px',
            'align'         => 'right'
        ));
        
        $this->addColumn('citation', array(
            'header'        => Mage::helper('citation')->__('Citation'),
            'index'         => 'citation',
            'filter_index'  => 'ct.citation',
            'align'         => 'center',
            'type'          => 'text',
            'width'         => '400px'
        ));

        /**
         * Check is single store mode
         */        
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'        => Mage::helper('citation')->__('Store View'),
                //display stores via index
                'index'         => 'store_id',
                'type'          => 'store',
                'store_view'    => true,
                'width'         => '125px',
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
            ));
        }
        
        $this->addColumn('date', array(
            'header'        => Mage::helper('citation')->__('Date of the publication'),
            'index'         => 'date_publication',
            'filter_index'  => 'ct.date_publication',
            'type'          => 'date'
        ));
        
        $this->addColumn('source', array(
            'header'        => Mage::helper('citation')->__('Source'),
            'index'         => 'source',
            'filter_index'  => 'ct.cource',
            'type'          => 'text'
        ));
        
        $this->addColumn('link', array(
            'header'        => Mage::helper('citation')->__('Link to the source'),
            'index'         => 'link',
            'filter_index'  => 'ct.link',
            'type'          => 'text'
        ));
    

        return parent::_prepareColumns();
    }
    
    /**
     * 
     * 
     */
    protected function _prepareMassaction()
    {
        //set massaction row identifier field
        $this->setMassactionIdField('citation_id');
        //retrive massaction block and set global form field name for all massaction items
        $this->getMassactionBlock()->setFormFieldName('citation');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'=> Mage::helper('citation')->__('Delete'),
             'url'  => $this->getUrl('*/*/massDelete'),
             'confirm' => Mage::helper('citation')->__('Are you sure?')
        ));
           
        
        return $this;
    }
    
     /**
     * Row click url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
         // This is where our row data will link to
        return $this->getUrl('*/*/edit', array('citation_id' => $row->getId()));
    }

}