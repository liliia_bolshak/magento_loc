<?php

class LB_Citation_Block_Adminhtml_Citation_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Create a form content block
     * The form item will be added by Tabs block
     */
    protected function _prepareForm()
    {
        $citation = Mage::registry('citation');
        //создаем объект типа Varien_Data_Form
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('citation_id' => $citation->getId())),
            'method' => 'post',
            'enctype' => 'multipart/form-data',
            ));
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}
