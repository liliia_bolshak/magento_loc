<?php

class LB_Citation_Block_Adminhtml_Citation_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('citation_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('citation')->__('Information'));
    }

    
}
