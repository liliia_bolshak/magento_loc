<?php


class LB_Citation_Block_Adminhtml_Citation extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * block grid container
     */
    public function __construct()
    {        
        $this->_blockGroup = 'citation';
        //where is the controller
        $this->_controller = 'adminhtml_citation';
        $this->_headerText = $this->__('Citation');
        $this->_addButtonLabel = $this->__('Add Citation');
        parent::__construct();
    }

}