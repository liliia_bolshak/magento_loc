<?php

class LB_Downloadtype_Helper_Image extends Mage_Core_Helper_Abstract
{
    //the default directory
    protected $_baseDirInMedia = 'downloadtype';
    
    public function getBaseDir()
    { 
        return Mage::getBaseDir('media') . DS .  $this->_baseDirInMedia;
    }
    
    public function getFileBaseUrl()
    {
         return Mage::getBaseUrl('media') . $this->_baseDirInMedia;
    }

}
