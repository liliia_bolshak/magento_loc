<?php


class LB_Downloadtype_Model_Observer
{
    /**
     * Add new field to form
     *
     * @param   Varien_Event_Observer $observer
     * intercept events "adminhtml_catalog_product_attribute_edit_prepare_form"
     */
    public function addFieldToForm(Varien_Event_Observer $observer)
    {
        $form = $observer->getEvent()->getForm();
        
       $fieldset = $form->getElement('base_fieldset');
       
        $fieldset->addField('path_to_download', 'text', array(
            'name' => 'path_to_download',
            'label' => Mage::helper('downloadtype')->__('Path to download'),
            'title' => Mage::helper('downloadtype')->__('Path to download'),
            'note' => Mage::helper('downloadtype')->__('Path to download (media/)'),
            
        ));
         
          $dependece = $form->getParent()->getChild('form_after')
            ->addFieldMap('frontend_input', 'frontend_input')           
            ->addFieldMap('path_to_download', 'path_to_download') 
            ->addFieldDependence('path_to_download', 'frontend_input', 'downloadtype');
           
        return $this;
        
    }
    
    /**
     * Add new attribute type to manage attributes interface
     *
     * @param   Varien_Event_Observer $observer
     * intercept events "adminhtml_product_attribute_types"
     */
    public function addAttributeType(Varien_Event_Observer $observer)
    {
        $response = $observer->getEvent()->getResponse();
        $types = $response->getTypes();
        $types[] = array(
            'value' => 'downloadtype',
            'label' => Mage::helper('downloadtype')->__('Download File'),
            'hide_fields' => array(
                'is_unique',
                'is_required',
                'frontend_class',
                'is_configurable',

                '_default_value',
                'is_filterable_in_search',
                'is_searchable',
                'is_visible_in_advanced_search',
                'is_comparable',
                'is_filterable',
                'is_used_for_promo_rules',
                'position',
                'used_for_sort_by',
            ),
            'disabled_types' => array(
                Mage_Catalog_Model_Product_Type::TYPE_GROUPED,
            )
        );

        $response->setTypes($types);

        
        return $this;
    }
    
    /**
     * Automaticaly assign backend model to downloadtype attributes
     *
     * @param   Varien_Event_Observer $observer
     * intercept events "catalog_entity_attribute_save_before"
     */
    public function assignBackendModelToAttribute(Varien_Event_Observer $observer)
    {
        $data = Mage::app()->getRequest()->getPost();
        $backendModel = LB_Downloadtype_Model_Attribute_Backend_Backendmodel::getBackendModelName();
        /** @var $object Mage_Eav_Model_Entity_Attribute_Abstract */
        $object = $observer->getEvent()->getAttribute();

        if ($object->getFrontendInput() == 'downloadtype') {
            $object->setBackendModel($backendModel);
            $object->setPathToDownload($data['path_to_download']);
           
            $object->setBackendType('varchar');
            
            if (!$object->getApplyTo()) {
                $applyTo = array();
                foreach (Mage_Catalog_Model_Product_Type::getOptions() as $option) {
                    if ($option['value'] == Mage_Catalog_Model_Product_Type::TYPE_GROUPED) {
                        continue;
                    }
                    $applyTo[] = $option['value'];
                }
                $object->setApplyTo($applyTo);
            }
        }

        return $this;
    }
    
    /** 
     * Add custom element type for attributes form
     *
     * @param   Varien_Event_Observer $observer
     * intercept events "adminhtml_catalog_product_edit_element_types"
     */
    public function updateElementTypes(Varien_Event_Observer $observer)
    {
        $response = $observer->getEvent()->getResponse();
        $types = $response->getTypes();
        $types['downloadtype'] = Mage::getConfig()->getBlockClassName('downloadtype/adminhtml_element_downloadtype');
        $response->setTypes($types);
        return $this;
    }

    /**
     * Automaticaly assign frontend model to downloadtype attributes
     *
     * @param   Varien_Event_Observer $observer
     * intercept events "catalog_entity_attribute_save_before"
     */
    public function assignFrontmodelToAttribute(Varien_Event_Observer $observer)
    {
    
        $frontModel = LB_Downloadtype_Model_Attribute_Frontend_Frontmodel::getFrontmodelName();
     
        $object = $observer->getEvent()->getAttribute();

        if($object->getFrontendInput() == 'downloadtype'){
            $object->setFrontendModel($frontModel);
            $object->setBackendType('varchar');
        }
        return $this;
    }
}



