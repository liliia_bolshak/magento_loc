<?php


class LB_Downloadtype_Model_Attribute_Backend_Backendmodel extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    
    public static function getBackendModelName()
    {
        return 'downloadtype/attribute_backend_backendmodel';
    }
    
     public function beforeSave($object)
     {
        
        $pathImage = $object->getData($this->getAttribute()->getName());
      
        //get source directory
        $value = $this->getAttribute()->getPathToDownload();
        
        if($value){
            $path = Mage::getBaseDir('media') . DS .  $value;
        }else{
            $path = Mage::helper('downloadtype/image')->getBaseDir();
        }

        //handling thumb image upload
        if (!empty($_FILES[$this->getAttribute()->getName()]['name'])) {
             
            try {
                $uploader = new Varien_File_Uploader($this->getAttribute()->getName());
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','zip','rar','tar'));
                /**
                 * If _allowRenameFiles variable is set to TRUE, uploaded file name will be changed if some
                 *file with the same name already exists in the destination directory (if enabled). 
                */ 
                $uploader->setAllowRenameFiles(true);
                //If this variable is set to TRUE, files despersion will be supported
                $uploader->setFilesDispersion(true);
               
                $results = $uploader->save($path, $_FILES[$this->getAttribute()->getName()]['name']);
                
                 
                /**
                 * If you choose to delete checkbox, the file must be physically removed
                 */
                //deleting previous file
                if (is_array($pathImage)) {
                    unlink($path . $pathImage['value']);
                } elseif (!empty($pathImage)) {
                    unlink($path . $pathImage);
                }

                //setting path to new uploade image 
                $object->setData($this->getAttribute()->getName(), $results['file']);
              
            } catch (Exception $e) {
                
            }
        } elseif (!empty($pathImage['delete']) && (!empty($pathImage['value']))) {
            unlink($path . $pathImage['value']);
            $object->setData($this->getAttribute()->getName(), '');
        } elseif (!empty($pathImage['value'])) {
            $object->setData($this->getAttribute()->getName(), $pathImage['value']);
        }
     }
    
}

