<?php

class LB_Downloadtype_Model_Attribute_Frontend_Frontmodel extends Mage_Eav_Model_Entity_Attribute_Frontend_Abstract
{
     public static function getFrontmodelName()
    {
        return 'downloadtype/attribute_frontend_frontmodel';
    }

    /**
     * Retreive attribute value
     *
     * @param $object
     * @return mixed
     */
    public function getValue(Varien_Object $object)
    {
        $html = '';
        //get attribute code
        $attributeCode = $this->getAttribute()->getAttributeCode();
    
        //get type
        $entity_type = $this->getAttribute()->getEntityTypeId();
        
        $catalogModel = Mage::getModel('catalog/resource_eav_attribute')->loadByCode($entity_type, $attributeCode);
        
        //get storage directory
        $dir = $catalogModel->getPathToDownload();

        $frontendInput = $catalogModel->getFrontendInput();
       
        if($frontendInput == 'downloadtype'){
            $file = $object->getData($this->getAttribute()->getAttributeCode());    
            
            if(preg_match("/\.(?:jp(?:e?g|e|2)|gif|png)$/i",$file)){
                
                if($dir){
                    $url = Mage::getBaseUrl('media') . $dir . $file;
                }else{
                    $url = Mage::helper('downloadtype/image')->getFileBaseUrl(). $file;
                }
                
                $html .= '<img src="'.$url.'" alt="'.$attributeCode.'" title="'.$attributeCode.'" height="150" width="150">';
                return $html;
            }else{
                if(!preg_match("/^http\:\/\/|https\:\/\//", $file) ) {
                
                    if($dir){
                        $url = Mage::getBaseUrl('media') . $dir . $file;
                    }else{
                        $url = Mage::helper('downloadtype/image')->getFileBaseUrl(). $file;
                    }
                
                }
                $html .= '<a href="'.$url.'" download>'."Download".'</a>';
                return $html;
            }
        }
        return parent::getValue($object);

    }


}
