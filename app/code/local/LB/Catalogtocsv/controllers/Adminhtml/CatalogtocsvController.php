<?php


class LB_Catalogtocsv_Adminhtml_CatalogtocsvController extends Mage_Adminhtml_Controller_Action
{
   
     protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('catalogtocsv')
            ->_addBreadcrumb(Mage::helper('catalogtocsv')->__('Uploading to CSV'), Mage::helper('catalogtocsv')->__('Uploading to CSV'))
            
        ;
        return $this;
    }

    /**
     * Index action
     */
    public function indexAction()
    {
       $this->_title(Mage::helper('catalogtocsv')->__('Uploading to CSV'));


        $this->_initAction();
        $this->renderLayout();
    } 
    
     public function newAction()
    {
        $this->_forward('edit');
    }
    /**
     * Edit Rules
     */
    public function editAction()
    {
        
        $this->_title(Mage::helper('catalogtocsv')->__('Rule'));

        //get ID and create model
        $id = $this->getRequest()->getParam('rule_id');
       
        $model = Mage::getModel('catalogtocsv/catalogtocsv');

        //initial checking
        if ($id) {
          
           $model->load($id);
            if (! $model->getRuleId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('catalogtocsv')->__('This rule no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }
       
        $model->getConditions()->setJsFormObject('rule_conditions_fieldset');
        
        //register model to use later in blocks
        Mage::register('catalogtocsv', $model);

        //build edit form
        $this->_initAction();

        $this->renderLayout();
        
 
    }
    
    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {

            try {
                $model = Mage::getModel('catalogtocsv/catalogtocsv');
                Mage::dispatchEvent(
                    'adminhtml_controller_catalogrule_prepare_save',
                    array('request' => $this->getRequest())
                );
                $data = $this->getRequest()->getPost();
        
                if ($id = $this->getRequest()->getParam('rule_id')) {
                    $model->load($id);
                    if ($id != $model->getRuleId()) {
                        Mage::throwException(Mage::helper('catalogrule')->__('Wrong rule specified.'));
                    }
                }

                $data['conditions'] = $data['rule']['conditions'];
                unset($data['rule']);

                $model->loadPost($data);

                Mage::getSingleton('adminhtml/session')->setPageData($model->getData());

                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('catalogrule')->__('The rule has been saved.')
                );
                Mage::getSingleton('adminhtml/session')->setPageData(false);
                
                //redirect to generate action
                if ($this->getRequest()->getParam('back')) {
                
                    $this->_redirect('*/*/generate', array('rule_id' => $model->getRuleId()));
                    return;
                }
                $this->_redirect('*/*/');
                
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('catalogrule')->__('An error occurred while saving the rule data. Please review the log and try again.')
                );
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $this->_redirect('*/*/edit', array('rule_id' => $this->getRequest()->getParam('rule_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }
    
     public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('rule_id')) {
            try {
                $model = Mage::getModel('catalogtocsv/catalogtocsv');
                $model->load($id);
                $model->delete();
                
                //delete file from dir
                $file = Mage::helper('catalogtocsv')->getFileDir(). '/' .$model->getFileName().'.csv';
                if(file_exists($file)){
                    unlink($file);
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('catalogrule')->__('The rule has been deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('catalogrule')->__('An error occurred while deleting the rule. Please review the log and try again.')
                );
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('rule_id' => $this->getRequest()->getParam('rule_id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('catalogtocsv')->__('Unable to find a rule to delete.')
        );
        $this->_redirect('*/*/');
    }
    
    /**
     * generate csv file
     */
     public function generateAction()
    {
        $id = $this->getRequest()->getParam('rule_id');
       
        // $data = $this->getRequest()->getPost();
        $model = Mage::getModel('catalogtocsv/catalogtocsv')->load($id);
        //$data['conditions'] = $data['rule']['conditions'];
        //        unset($data['rule']);

        //$model->loadPost($data);
    
        $ids = $model->getMatchingProductIds();
        $filename = $model->getFileName().'.csv';

        Mage::helper('catalogtocsv')->setList($ids);

        $content = Mage::helper('catalogtocsv')->generateList($filename);
       
        $this->_prepareDownloadResponse($filename, $content);
   
    }
    
    /**
     * Remove group rules
     */
     public function massDeleteAction()
    {
        $ruleIds = $this->getRequest()->getParam('catalogtocsv');
     
        if (!is_array($ruleIds)) {
            $this->_getSession()->addError($this->__('Please select rules'));
        } else {
            if (!empty($ruleIds)) {
                try {
                    foreach ($ruleIds as $ruleId) {
                        $rule = Mage::getModel('catalogtocsv/catalogtocsv')->load($ruleId);
                        $rule->delete();
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been deleted.', count($ruleIds))
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }
    

}