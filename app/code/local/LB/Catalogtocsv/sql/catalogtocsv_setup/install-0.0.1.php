<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'catalogtocsv/rule_for_csv'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('catalogtocsv/rule_for_csv'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
           'nullable'  => true,
           'identity'  => true,
           'primary'   => true,
        ), 'rule_id')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'Rule')
    ->addColumn('conditions_serialized', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        ), 'Conditions Serialized')
    ->addColumn('file_name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'File Name')
        
    ->addColumn('website_ids', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        ), 'Website Ids')
//    ->addIndex(
//            $installer->getIdxName('catalogtocsv/rule_for_csv', array('website_id')),
//            array('website_id')
//        )
//    ->addForeignKey($installer->getFkName('catalogtocsv/rule_for_csv', 'website_id', 'core/website', 'website_id'),
//            'website_id', $installer->getTable('core/website'), 'website_id',
//            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
//        )
    ;
$installer->getConnection()->createTable($table);
$installer->endSetup();
