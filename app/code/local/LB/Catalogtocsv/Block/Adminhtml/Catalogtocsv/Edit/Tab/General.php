<?php


class LB_Catalogtocsv_Block_Adminhtml_Catalogtocsv_Edit_Tab_General
   extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('catalogtocsv')->__('General');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('catalogtocsv')->__('General');
    }

    /**
     * Returns status flag about this tab can be showed or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    protected function _prepareForm()
    {
        $model = Mage::registry('catalogtocsv');

        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('rule_');

        $fieldset = $form->addFieldset('base_fieldset',
            array('legend '=> Mage::helper('catalogtocsv')->__('General Information'))
        );

        if ($model->getRuleId()) {
            $fieldset->addField('rule_id', 'hidden', array(
                'name' => 'rule_id',
            ));
        }

        $fieldset->addField('title', 'text', array(
            'name' => 'title',
            'label' => Mage::helper('catalogtocsv')->__('Rule Name'),
            'title' => Mage::helper('catalogtocsv')->__('Rule Name'),
            'required' => true,
        ));

        $fieldset->addField('file_name', 'text', array(
            'name' => 'file_name',
            'label' => Mage::helper('catalogtocsv')->__('File Name'),
            'title' => Mage::helper('catalogtocsv')->__('File Name'),
            'required' => true,
        ));
        $file = Mage::helper('catalogtocsv')->getFileDir(). '/' .$model->getFileName().'.csv';
        if(file_exists($file)){
        $fieldset->addField('link', 'link', array(
            'label' => Mage::helper('catalogtocsv')->__('Link To Download File'),
            'title' => Mage::helper('catalogtocsv')->__('CSV File'),
           //'style'   => "",
            'href' => Mage::helper('catalogtocsv')->getFileUrl(). '/' .$model->getFileName().'.csv',
            'value'  => Mage::helper('catalogrule')->__('CSV File'),
            'after_element_html' => '',            
        ));
        }

         if (Mage::app()->isSingleStoreMode()) {
            $websiteId = Mage::app()->getStore(true)->getWebsiteId();
            $fieldset->addField('website_ids', 'hidden', array(
                'name'     => 'website_ids',
                'value'    => $websiteId
            ));
            $model->setWebsiteIds($websiteId);
        } else {
            $field = $fieldset->addField('website_ids', 'multiselect', array(
                'name'     => 'website_ids',
                'label'     => Mage::helper('catalogrule')->__('Websites'),
                'title'     => Mage::helper('catalogrule')->__('Websites'),
                'required' => true,
                'values'   => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm()
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }

        $form->addValues($model->getData());

        $this->setForm($form);


        return parent::_prepareForm();
    }
}
