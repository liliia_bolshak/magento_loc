<?php


class LB_Catalogtocsv_Block_Adminhtml_Catalogtocsv_Grid_Renderer_Action
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * 
     * make reference to csv file
     * @return type
     */
    public function render(Varien_Object $row)
    {

        $href = Mage::helper('catalogtocsv')->getFileUrl(). '/' .$row->getFileName().'.csv';
      
        return '<a href="'.$href.'" download>'.$this->__('CSV File').'</a>';
    }
}
