<?php

class LB_Catalogtocsv_Block_Adminhtml_Catalogtocsv_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Create a form container block
     *
     * @return void
     */
    public function __construct()
    {
        $this->_objectId   = 'rule_id';
        $this->_blockGroup = 'catalogtocsv';
        $this->_controller = 'adminhtml_catalogtocsv';
        $model = Mage::registry('catalogtocsv');
        parent::__construct();

        if ($this->_isAllowedAction('save')) {
            $this->_updateButton('save', 'label', Mage::helper('catalogtocsv')->__('Save Rule'));
                     
            if($model->getRuleId()){
            $this->_addButton('generate', array(
                'label'     => Mage::helper('catalogtocsv')->__('Generate'),
                'onclick'   => 'editForm.submit(\''. Mage::getUrl('*/*/generate', array('id' => $model->getRuleId())).'\')',             
            ), -101);         
            }
            $this->_addButton('saveandgenerate', array(
               'label'     => Mage::helper('catalogtocsv')->__('Save and Generate'),
               'onclick'   => 'editForm.submit($(\'edit_form\').action + \'back/edit/\')',
            ), -102);
            
        } else {
            $this->_removeButton('save');
        }

        if ($this->_isAllowedAction('delete')) {
            $this->_updateButton('delete', 'label', Mage::helper('catalogtocsv')->__('Delete Rule'));
        } else {
            $this->_removeButton('delete');
        }
        
    }

    /**
     * Retrieve text for header element depending on loaded page
     */
    public function getHeaderText()
    {
        if (Mage::registry('catalogtocsv')->getId()) {
            return Mage::helper('catalogtocsv')->__("Edit '%s'", $this->escapeHtml(Mage::registry('catalogtocsv')->getTitle()));
        }
        else {
            return Mage::helper('catalogtocsv')->__('New Rule');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalogtocsv/' . $action);
    }


  
}
