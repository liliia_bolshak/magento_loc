<?php

class LB_Catalogtocsv_Block_Adminhtml_Catalogtocsv_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        
        parent::__construct();
        $this->setId('catalogtocsvGrid');
        $this->_controller = 'adminhtml_catalogtocsv';
        $this->setDefaultDir('ASC');
    }
    
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalogtocsv/catalogtocsv')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
   
    protected function _prepareColumns()
    {
        $dateFormatIso = Mage::app()->getLocale() ->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
        // Add the columns that should appear in the grid
        $baseUrl = $this->getUrl();
        $this->addColumn('rule_id', array(
            'header'        => Mage::helper('catalogtocsv')->__('ID'),
            'align'         => 'right',
            'width'         => '20px',
            'index'         => 'rule_id'
        ));
        $this->addColumn('title', array(
            'header'    => Mage::helper('catalogtocsv')->__('Rule'),
            'align'     => 'left',
            'index'     => 'title',
        ));
        
        $this->addColumn('link', array(
            'header'    => Mage::helper('catalogtocsv')->__('CSV'),
            'width'     => '100',
            'align'     => 'center',
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'catalogtocsv/adminhtml_catalogtocsv_grid_renderer_action',
        ));
              
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('catalogtocsv')->__('Action'),
                'width'     => '100',
                'align'     => 'center',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('catalogtocsv')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'rule_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));

        return parent::_prepareColumns();
    }
    
   
     /**
     * 
     * 
     */
    protected function _prepareMassaction()
    {
        //set massaction row identifier field
        $this->setMassactionIdField('rule_id');
        //retrive massaction block and set global form field name for all massaction items
        $this->getMassactionBlock()->setFormFieldName('catalogtocsv');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'=> Mage::helper('catalogtocsv')->__('Delete'),
             'url'  => $this->getUrl('*/*/massDelete'),
             'confirm' => Mage::helper('catalogtocsv')->__('Are you sure?')
        ));
           
        return $this;
    }
     /**
     * Row click url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
         // This is where our row data will link to
        return $this->getUrl('*/*/edit', array('rule_id' => $row->getRuleId()));
    }

}