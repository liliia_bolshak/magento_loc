<?php

class LB_Catalogtocsv_Block_Adminhtml_Catalogtocsv_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('catalogtocsv_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('catalogtocsv')->__('General'));
    }

}
