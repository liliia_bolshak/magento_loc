<?php

class LB_Catalogtocsv_Block_Adminhtml_Catalogtocsv_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Create a form content block
     * The form item will be added by Tabs block
     */
    protected function _prepareForm()
    {
        $catalogtocsv = Mage::registry('catalogtocsv');
        //создаем объект типа Varien_Data_Form
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('rule_id' => $catalogtocsv->getRuleId())),
            'method' => 'post',
           
            ));
       // $form = new Varien_Data_Form(array('id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post'));
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}
