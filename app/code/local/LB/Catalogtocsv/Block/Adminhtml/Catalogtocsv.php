<?php


class LB_Catalogtocsv_Block_Adminhtml_Catalogtocsv extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * block grid container
     */
    public function __construct()
    {        
        $this->_blockGroup = 'catalogtocsv';
        //where is the controller
        $this->_controller = 'adminhtml_catalogtocsv';
        $this->_headerText = $this->__('Uploading to CSV');
        $this->_addButtonLabel = $this->__('Add Rule');
        parent::__construct();
    }

}