<?php
class LB_Catalogtocsv_Helper_Data extends Mage_Core_Helper_Abstract
{
     /**
     * Contains current collection
     * @var string
     */
    protected $_list = null;
    public $dir = 'export';
    

    public function getFileUrl(){
       return Mage::getBaseUrl('media') . $this->dir;
    }
   
        public function getFileDir(){
       return Mage::getBaseDir('media') . DS . $this->dir;
    }
    /**
     * Sets current collection
     * @param $query
     */
    public function setList($collection)
    {
        $this->_list = $collection;
    }
 
    /**
     * Returns indexes of the fetched array as headers for CSV
     * @param array $products
     * @return array
     */
    protected function _getCsvHeaders($products)
    {
        $product = current($products);
        $headers = array_keys($product);
 
        return $headers;
    }
    
    /**
     * Generates CSV file with product's list according to the collection in the $this->_list
     * @return array
     */
    public function generateList($name)
    { 
        if (!is_null($this->_list)) {
            $items = $this->_list;

            if (count($items) > 0) {

                $io = new Varien_Io_File();
                $path = Mage::getBaseDir('media') . DS . $this->dir . DS;
                
                //$name = md5(microtime());
                $file = $path . DS . $name;
               
                $io->setAllowCreateFolders(true);
                $io->open(array('path' => $path));
                $io->streamOpen($file, 'w+');
                $io->streamLock(true);
 
                $io->streamWriteCsv($this->_getCsvHeaders($items));
                foreach ($items as $product) {
                    $io->streamWriteCsv($product);
                }
 
                return array(
                    'type'  => 'filename',
                    'value' => $file,
                    'rm'    => false // can delete file after use
                );
            }
        }
    }

}
