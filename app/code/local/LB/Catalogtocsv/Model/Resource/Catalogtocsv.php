<?php
class LB_Catalogtocsv_Model_Resource_Catalogtocsv extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('catalogtocsv/rule_for_csv', 'rule_id');
    }
   
}
