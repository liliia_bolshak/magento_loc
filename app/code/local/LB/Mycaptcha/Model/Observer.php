<?php

/**
 * Description of Observer
 *
 * @author denis
 */
class LB_Mycaptcha_Model_Observer
{
    public function checkCaptcha($observer)
    {
      $formId = 'form-validate-captcha';
        $captchaModel = Mage::helper('captcha')->getCaptcha($formId);
        if ($captchaModel->isRequired()) {
            $controller = $observer->getControllerAction();
//            $post = $controller->getRequest()->getPost('name');
//            $_captcha = Mage::getSingleton('customer/session')->setData($post);
            //var_dump($_captcha);die();
            if (!$captchaModel->isCorrect($this->_getCaptchaString($controller->getRequest(), $formId))) {
                
               
                Mage::getSingleton('core/session')->addError(Mage::helper('captcha')->__('Incorrect CAPTCHA.'));
                $controller->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
             
                $controller->getResponse()->setRedirect(Mage::getUrl('*/*/'));
            }
        }
        return $this;
    }
    
     /**
     * Get Captcha String
     *
     * @param Varien_Object $request
     * @param string $formId
     * @return string
     */
    protected function _getCaptchaString($request, $formId)
    {
        $captchaParams = $request->getPost(Mage_Captcha_Helper_Data::INPUT_NAME_FIELD_VALUE);
        return $captchaParams[$formId];
    }
}
