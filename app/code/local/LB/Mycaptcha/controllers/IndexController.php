<?php

class LB_Mycaptcha_IndexController extends Mage_Core_Controller_Front_Action {

   
    const XML_PATH_ENABLED = 'mycaptcha/mycaptcha/enabled';
    
    /**
     * should set layout area
     */
    public function preDispatch() {
        parent::preDispatch();

        if (!Mage::getStoreConfigFlag(self::XML_PATH_ENABLED)) {
            $this->norouteAction();
        }
    }

    /**
     * 
     */
    public function indexAction() {
        //load layout by handles
        $this->loadLayout();
   
       // $fieldName = Mage::getSingleton('customer/session')->getData('name');
        
        //retrieve current layout object and get block object by name
        $this->getLayout()->getBlock('form.with.captcha')
                ->setFormAction(Mage::getUrl('*/*/post'));

        //initializing layout messages by message storage(s), loading and adding messages to layout messages block
        $this->_initLayoutMessages('core/session');
        //rendering layout
        $this->renderLayout();
    }

    public function postAction() {
 
        $post = $this->getRequest()->getPost();
        if ($post) {

            /* @var $translate Mage_Core_Model_Translate */
          //  $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                //returns true if $value meets the validation requirements
                if (!Zend_Validate::is(trim($post['name']), 'NotEmpty')) {

                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }



                $fileName = '';

                $path = Mage::getBaseDir('media') . DS . 'sendmodule' . DS;


                if (isset($_FILES['attachment']['name']) && $_FILES['attachment']['name'] != '') {
                    try {


                        $uploader = new Varien_File_Uploader('attachment');
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png', 'zip', 'txt'));
                        //if this variable is set to TRUE, uploaded file name will be changed if some file with the same
                        // name already exists in the destination directory (if enabled).
                        $uploader->setAllowRenameFiles(true);
                        //if this variable is set to TRUE, files despersion will be supported
                        $uploader->setFilesDispersion(true);
                        //used to save uploaded file into destination folder with original or new file name
                        $uploader->save($path, $_FILES['attachment']['name']);
                        //returns a name of uploaded file
                        $newFilename = $uploader->getUploadedFileName();
                    } catch (Exception $e) {
                        echo 'Error Message: ' . $e->getMessage();
                    }
                }

                if ($error) {

                    throw new Exception();
                }

                //load email template
             
                $transactionalEmail = Mage::getModel('core/email_template');
                $templateId = $transactionalEmail->loadDefault('mycaptcha_email_email_template');
                $attachmentFilePath = Mage::getBaseDir('media') . DS . 'mycaptcha' . $newFilename;

                if (file_exists($attachmentFilePath)) {
                    //reads entire file into a string
                    $fileContents = file_get_contents($attachmentFilePath);

                    /* retrieve mail object instance
                    */
                    $transactionalEmail->getMail()
                            ->createAttachment(
                                    $fileContents, 
                                    Zend_Mime::TYPE_OCTETSTREAM,
                                    Zend_Mime::DISPOSITION_ATTACHMENT,
                                    Zend_Mime::ENCODING_BASE64,
                                    basename($newFilename)
                    );
                }
                
                //send file
                $transactionalEmail
                         ->sendTransactional(
                                $templateId, 
                                Array('name' => $post['name'], 'email' => $post['email']),
                                Mage::getStoreConfig('trans_email/ident_general/email'),
                                null,
                                  array('data' => $postObject) );


                if (!$transactionalEmail->getSentSuccess()) {

                    throw new Exception();
                }


                //set success message for front page
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('mycaptcha')->__('Your file was sended successfully and we will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {


                //set error message for front page :
                Mage::getSingleton('core/session')->addError(Mage::helper('mycaptcha')->__('Unable to submit your request. Please, try again later'));

                $this->_redirect('*/*/');
                return;
            }
        } else {
            $this->_redirect('*/*/');
        }
    }
    
    

}
