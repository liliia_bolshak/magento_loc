<?php
class LB_Navsettings_Model_Observer extends Mage_Catalog_Model_Observer
{
     /**
     * Adds catalog categories to top menu
     *
     * @param Varien_Event_Observer $observer
     */
    public function addCatalogToTopmenuItems(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();
 
        $menu = $observer->getMenu();
        $tree = $menu->getTree();
 
        $node = new Varien_Data_Tree_Node(array(
                'name'   => 'Catalog',
                'id'     => 'catalog',
                'url'    => Mage::getUrl(), 
        ), 'id', $tree, $menu);
 
        $menu->addChild($node);
         $this->_addCategoriesToMenu(
            Mage::helper('catalog/category')->getStoreCategories(), $node, $block, true
        );
 
    }

    /**
     * Recursively adds categories to top menu
    
     */
    protected function _addCategoriesToMenu($categories, $parentCategoryNode, $menuBlock, $addTags = false)
    {
        $categoryModel = Mage::getModel('catalog/category');
        foreach ($categories as $category) {
            if (!$category->getIsActive()) {
                continue;
            }

            $nodeId = 'category-node-' . $category->getId();

            $categoryModel->setId($category->getId());
            
            if ($addTags) {
                $menuBlock->addModelTags($categoryModel);
            }

            $tree = $parentCategoryNode->getTree();
                   
            $categoryData = array(
                'name' => $category->getName(),
                'id' => $nodeId,
                'category_id' => $category->getId(),
                'url' => Mage::helper('catalog/category')->getCategoryUrl($category),
                'is_active' => $this->_isActiveMenuCategory($category),
                'additional_item_classes' => $categoryModel->load($category->getId())->getAdditionalItemClasses(),
            );

            $categoryNode = new Varien_Data_Tree_Node($categoryData, 'id', $tree, $parentCategoryNode);
            
            $parentCategoryNode->addChild($categoryNode);

            $flatHelper = Mage::helper('catalog/category_flat');
            if ($flatHelper->isEnabled() && $flatHelper->isBuilt(true)) {
                $subcategories = (array)$category->getChildrenNodes();
                 
            } else {
                $subcategories = $category->getChildren();
               
            }

            $this->_addCategoriesToMenu($subcategories, $categoryNode, $menuBlock, $addTags);
        }
    }
    
    
}
