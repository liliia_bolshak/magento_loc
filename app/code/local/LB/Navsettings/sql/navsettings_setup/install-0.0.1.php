<?php



$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
//$setup->removeAttribute( 'catalog_category', 'replacement_content' );
$setup->addAttributeGroup('catalog_category', 'Default', 'Additional Nav Menu Settings', 1000);

$setup->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'new_column_after', array(
    'type'          => 'int',
    'input'         => 'select',
    'label'         => 'Nav menu new column after',
    'source'        => 'eav/entity_attribute_source_boolean',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => true,
    'group'         => 'Additional Nav Menu Settings',
));

$setup->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'output_link', array(
    'type'          => 'int',
    'input'         => 'select',
    'label'         => 'Nav menu output link',
    'source'        => 'eav/entity_attribute_source_boolean',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => true,
    'group'         => 'Additional Nav Menu Settings',
));

$setup->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'additional_item_classes', array(
    'type'          => 'text',
    'input'         => 'text',
    'label'         => 'Nav menu additional item classes',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => true,
    'required'      => false,
    'user_defined'  => true,
    'group'         => 'Additional Nav Menu Settings',
));

$setup->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'replacement_content', array(
    'group'         => 'Additional Nav Menu Settings',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'Nav menu replacement content',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    
    'visible_on_front' => true,
   
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$setup->updateAttribute('catalog_category', 'replacement_content', 'is_wysiwyg_enabled', 1);
$setup->updateAttribute('catalog_category', 'replacement_content', 'is_html_allowed_on_front', 1);

$installer->endSetup();
?>