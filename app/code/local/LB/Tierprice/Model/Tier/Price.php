<?php


class LB_Tierprice_Model_Tier_Price extends Mage_Catalog_Model_Product_Type_Configurable_Price
{

    protected function _applyTierPrice($product, $qty, $finalPrice)
    {
        //get total quantity of items of the conf product
        $qty  = $this->_calculateQtyItemsProduct($product,$qty);
        
        $tierPrice  = $product->getTierPrice($qty);
        if (is_numeric($tierPrice)) {           
            $finalPrice = min($finalPrice, $tierPrice);
        }
        return $finalPrice;
    }
    
    private function _calculateQtyItemsProduct($product,$qty)
    {
        $productId = $product->getId();
        $totalQty = 0;
        //getAllVisibleItems will show only parent items
        if ($items = Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems()) {
            foreach ($items as $item) {
                if ($productId == $item->getProductId()) {
                    $totalQty += $item->getQty();
                }                
            }
        }

        return $totalQty ? $totalQty : $qty;       
    }
   
}