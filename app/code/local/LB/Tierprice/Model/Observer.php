<?php


class LB_Tierprice_Model_Observer
{
    public function getFinalPrice(Varien_Event_Observer $observer)
    {
        $product = $observer->getProduct();
        if ($product->isConfigurable()) {
            // if tier prices are defined, also adapt them to configurable products           
            if ($product->getTierPriceCount() > 0) {
                $tierPrice = $this->_calculateConfProductTierPrice($product);
                if ($tierPrice < $product->getData('final_price')) {
                    $product->setData('final_price', $tierPrice);
                }            
            } 
        return $this;
        }
        return $this;    
    }
    
    private function _calculateConfProductTierPrice($product)
    {
        $tierPrice = $product->getData('final_price');
        //getAllVisibleItems will show you only products that have the parent
        if ($items = Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems()) {
            //IDs of the parent products with the quantities of the corresponding simple products
            $idQty = array();
            // go through all products in the quote
            foreach ($items as $item) {
                // get parent product ID
                $id = $item->getProductId();
                // array with the parent ID and the quantity of the simple product
                $idQty[$id][] = $item->getQty();               
            }
            
            // calculate the total quantity of items of the conf producte
            if (array_key_exists($product->getId(), $idQty)) {
                $totalQty  = array_sum($idQty[$product->getId()]);
                $tierPrice = $product->getPriceModel()->getBasePrice($product, $totalQty);
            }
        }
        return $tierPrice;        
    }
}
