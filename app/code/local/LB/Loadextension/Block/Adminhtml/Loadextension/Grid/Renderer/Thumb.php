<?php

class LB_Loadextension_Block_Adminhtml_Loadextension_Grid_Renderer_Thumb
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * 
     */
    public function render(Varien_Object $row)
    {
       if($row['image']){
        $helper = Mage::helper('loadextension/image');
        $helper->init($row, 'image')
            ->keepAspectRatio(true);
       
        $url = $helper->resize(350,350);
             
   
        return '<a href="#" onclick="showPopup(\'' . $url . '\')" class="imagePrev"><img src="' . $helper->resize(200,200) . '" /></a>';
        //return '<a href="#" class="imagePrev" data-url="' . $url . '"><img src="' . $helper->resize(200,200) . '" /></a>';
       }
    }
}
