<?php

class LB_Loadextension_Block_Adminhtml_Loadextension_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    const TYPE_FILE = 'file';
    const TYPE_DIR = 'dir';

    public function __construct()
    {
        
        parent::__construct();
        $this->setId('loadextensionGrid');
        $this->_controller = 'adminhtml_loadextension';
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(TRUE);
       
    }
    
    /**
     * Creates collection if it has not been created yet
     *
     * 
     */
    public function getCollection()
    {
        if (!$this->_collection) {
            $this->_collection = Mage::getModel('loadextension/loadextension_collection');
        }
        
        return $this->_collection;
    }

    /**
     * Prepare Local Package Collection for Grid
     *
     * @return Mage_Connect_Block_Adminhtml_Extension_Custom_Edit_Tab_Grid
     */
    protected function _prepareCollection()
    {
        $this->setCollection($this->getCollection());
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
               
        $this->addColumn('title', array(
            'header'    => Mage::helper('loadextension')->__('Title'),
            'align'     => 'left',
            'index'     => 'name',
           // 'filter_condition_callback' => array($this, 'filterCallbackLike'),
           
        ));

          $this->addColumn('type', array(
            'header'    => Mage::helper('loadextension')->__('Type'),
            'align'     => 'left',
            'index'     => 'type',
            'type'      => 'options',
            'options'   => array(
                self::TYPE_FILE => 'file',
                self::TYPE_DIR  => 'dir',
                ),
        ));
          
        $this->addColumn('size', array(
            'header'    => Mage::helper('loadextension')->__('Size'),
            'align'     => 'left',
            'index'     => 'size',
            'sortable'  => false,
        ));
        $this->addColumn('time', array(
            'header'    => Mage::helper('loadextension')->__('Update Time'),
            'index'     => 'time',
            'type'      => 'date',
            'sortable'  => true,
            //'format' => $dateFormatIso,
        ));
                     
        $this->addColumn('image', array(
            'header'    => Mage::helper('loadextension')->__('Thumb image'),
            'align'     => 'center',
            'index'     => 'image',
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'loadextension/adminhtml_loadextension_grid_renderer_thumb',
        ));
        
        return parent::_prepareColumns();
    }
   
    
}
