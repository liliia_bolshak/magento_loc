<?php


class LB_Loadextension_Block_Adminhtml_Loadextension extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * block grid container
     */
    public function __construct()
    {   
        $this->_blockGroup = 'loadextension';
        //where is the controller
        $this->_controller = 'adminhtml_loadextension';
        $this->_headerText = $this->__('Loadextension');
    
        parent::__construct();
        $this->removeButton('add');
    }

}