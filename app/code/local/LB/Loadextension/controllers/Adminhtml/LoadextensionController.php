<?php


class LB_Loadextension_Adminhtml_LoadextensionController extends Mage_Adminhtml_Controller_Action
{
    
    /**
     * 
     *  this method will set some basic params for each action

     */
     protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('loadextension')
            ->_addBreadcrumb(Mage::helper('loadextension')->__('Loadextension'), Mage::helper('loadextension')->__('Loadextension'))
            
        ;
        return $this;
    }

    /**
     * Index action
     */
    public function indexAction()
    {
       $this->_title($this->__('Loadextension'));


        $this->_initAction();
        $this->renderLayout();
    }

}