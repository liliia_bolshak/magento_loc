<?php

class LB_Loadextension_Model_Loadextension_Collection extends Varien_Data_Collection_Filesystem
{
    protected $_currentDir;
    //protected $_isFiltersRendered = true;
    protected $_collectRecursively = true;
    //protected $_collectDirs = true;
    
    public function __construct()
    {
        
        $this->_currentDir = Mage::getStoreConfig('loadextension/loadfolder/loadfolder');
        $this->addTargetDir($this->_currentDir);
        $this->setCollectDirs(true);
        
               
    }

    /**
     * Row generator
     *
     * @param string $filename
     * @return array
     */
    protected function _generateRow($filename)
    {
        $row = parent::_generateRow($filename);
     
        $row['name'] = basename($filename);
        $row['size'] = filesize($filename);
        $row['time'] =  date("F j, Y, g:i a", filemtime($filename));
        
        $row['type'] = filetype($filename);
        if(preg_match("/\.(?:jp(?:e?g|e|2)|gif|png|tiff?|bmp|ico)$/i",basename($filename))){
            $row['image'] = basename($filename);
        }
        
                
        return $row;
    }
    
    public function filterCallbackLike($field, $filterValue, $row)
    {
            
        if (strpos($filterValue, '\'') === 0){
           $filterValue = substr($filterValue, 1, strlen($filterValue) - 2);
        }

        return parent::filterCallbackLike($field, $filterValue, $row);
    }
    
}
