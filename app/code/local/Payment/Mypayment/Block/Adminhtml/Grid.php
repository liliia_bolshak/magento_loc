<?php

class Payment_Mypayment_Block_Adminhtml_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        
        parent::__construct();
        $this->setId('mypaymentGrid');
        $this->_controller = 'adminhtml_mypayment';
        $this->setUseAjax(true);
        $this->setDefaultDir('ASC');//sorting
    }

    /**
     * 
     * prepare collection
     */
    protected function _prepareCollection()
    {
        //get and set our collection for the grid
        $collection = Mage::getModel('mypayment/mypaymenthistory')->getCollection();
        //get customer id from the request
        if ($this->getRequest()->getParam('id', false)){
            $customerId = $this->getRequest()->getParam('id');
        }
        //filter the collection
        $collection->addCustomerFilter($customerId);
        
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }

    /**
     * 
     * prepare grid's columns
     */
    protected function _prepareColumns()
    {
        $dateFormatIso = Mage::app()->getLocale() ->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
        
        // Add the columns that should appear in the grid
       
        $this->addColumn('order_id', array(
            'header'    => Mage::helper('mypayment')->__('Order #'),
            'align'     => 'left',
            'index'     => 'order_id',
        ));
        $this->addColumn('grand_total', array(
            'header'    => Mage::helper('mypayment')->__('Order total'),
            'index'     => 'grand_total',
            'type'      => 'currency',
            'align'     => 'right',
        ));
       
        $this->addColumn('currency_type', array(
            'header'    => Mage::helper('mypayment')->__('Type of currency'),
            'index'     => 'currency_type',
        ));
        
         $this->addColumn('date', array(
            'header'    => Mage::helper('mypayment')->__('Date Added'),
            'align'     => 'left',
            'index'     => 'date',
            'type'      => 'datetime',
            'format'    => $dateFormatIso,
        ));
        
        $this->addColumn('state', array(
            'header'    => Mage::helper('mypayment')->__('Status'),
            'index'     => 'state',
            'type'      => 'options',
            'options'   => array(
                'open'          => Mage::helper('sales')->__('Pending'),
                'refunded'      => Mage::helper('sales')->__('Refunded'),
                'canceled'      => Mage::helper('sales')->__('Canceled'),
                'complete'      => Mage::helper('sales')->__('Complete'),
                'processing'    => Mage::helper('sales')->__('Processing'),
                'paid'          => Mage::helper('sales')->__('Paid'),
               )
           
        ));
        return parent::_prepareColumns();
    }



    
   
}
