<?php


class Payment_Mypayment_Block_Adminhtml_Mypaymenthistory extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * block grid container
     */
    public function __construct()
    {        
        $this->_blockGroup = 'mypayment';
        //where is the controller
        $this->_controller = 'adminhtml_mypayment';
        parent::__construct();
    }

}