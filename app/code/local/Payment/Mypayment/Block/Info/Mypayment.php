<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author denis
 */
class Payment_Mypayment_Block_Info_Mypayment extends Mage_Payment_Block_Info
{
    protected function _construct() 
    {
        parent::_construct();
        $this->setTemplate('mypayment/info/mypayment.phtml');
    }
    
    /**
     * 
     * Prepare information specific to current payment method
     */
     protected function _prepareSpecificInformation($transport = null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }
        //Retrieve info model
        $info = $this->getInfo();
        
        $transport = new Varien_Object();
        //Prepare information specific to current payment method
        $transport = parent::_prepareSpecificInformation($transport);
        $transport->addData(array(
            Mage::helper('payment')->__('Comments') => $info->getComments()
        ));
        return $transport;
    }

}
