<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mypayment
 *
 * @author denis
 */

class Payment_Mypayment_Model_Mypayment extends Mage_Payment_Model_Method_Abstract
{
    
    //unique internal payment method identifier
    protected $_code = 'mypayment';
    //this variable has the path of the block class which contains form
    protected $_formBlockType = 'mypayment/form_mypayment';
    protected $_infoBlockType = 'mypayment/info_mypayment';
    
    //flag if we need to run payment initialize while order place
    protected $_isInitializeNeeded      = true;
    
    //can use this payment method in administration panel?
    protected $_canUseInternal          = false;
    
    //can show this payment method as an option on checkout payment page?
    protected $_canUseCheckout          = true;
    
    //is this payment method suitable for multi-shipping checkout?
    protected $_canUseForMultishipping  = false;
    
    
    /**
     * 
     * Retrieve payment method title
     */
    public function getTitle()
    {
        $cTitle = $this->getConfigData('title');
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        //get currency symbol
        $currencySymbol = Mage::app()->getLocale()->currency($quote->getQuoteCurrencyCode())->getSymbol();
        
        if (!$quote->getId()) {  
            return $cTitle;
        }
        //sets the number of decimal points
        $balanse = number_format($quote->getCustomer()->getCreditBalans(),2);
        
        return $cTitle . ' (' . $balanse . $currencySymbol . ' )';
   }
    
    /**
     * Check whether payment method can be used
     */
    public function isAvailable()
    { 
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $grandtotalAmount = $quote->getGrandTotal();
        $balanse = $quote->getCustomer()->getCreditBalans();
         
        if( $grandtotalAmount < $balanse){
            if($this->getCode() == 'mypayment' ){
                return true;
            }else{
                return false;
            }
        
        }
    }
    
    /**
     * 
     *Assign data to info model instance
     */
    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        
        //retrieve payment iformation model object
        $info = $this->getInfoInstance();
        
        //set data to the attribute
        $info->setComments($data->getComments());
       
        return $this;
    }
 
    /**
     * 
     * validate payment method information object
     */
    public function validate()
    {
        parent::validate();
 
        $info = $this->getInfoInstance();
 
        $comments = $info->getComments();
        
        if(empty($comments)){
            $errorCode = 'invalid_data';
            $errorMsg = $this->_getHelper()->__('Comments are required fields');
        }
 
        if($errorMsg){
            Mage::throwException($errorMsg);
        }
        return $this;
    }

}
