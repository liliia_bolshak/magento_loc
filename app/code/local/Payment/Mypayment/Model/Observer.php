<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Observer
 *
 * @author denis
 */
class Payment_Mypayment_Model_Observer
{
    const CUSTOMER_TABS_BLOCK_NAME = 'customer_edit_tabs';
    
    /**
     * 
     * set the credit balance for registered customers
    */
    public function customerRegisterSuccess(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        $customer = $event->getCustomer()
                          ->setCreditBalans('300')
                          ->save();
    }
    
    /**
     * changes the customer's credit balance when the customer makes a purchase
     * 
     */
    public function chengeCreditBalans($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        if ($quote->getPayment()->getMethodInstance()->getCode() == 'mypayment'){
            $customer = $quote->getCustomer();
            $creditBalans = $customer->getCreditBalans();
            $total = $quote->getGrandTotal();
            $balanse = $creditBalans - $total;
            $newBalanse = $customer->setCreditBalans($balanse);
            $customer->save();
        }
    }
    
    /**
     * 
     * add data to 'mypayment_history' if the customer uses mypayment method
     */
    public function setPaymentHistory($observer)
    { 
        $order = $observer->getEvent()->getOrder();
        $customer = $order->getCustomer()->getId();
        $status = $order->getState();
        $currencyName = Mage::app()->getLocale()->currency($order->getOrderCurrencyCode())->getShortName();
        
        if ($order->getPayment()->getMethodInstance()->getCode() == 'mypayment'){

            $model = Mage::getModel('mypayment/mypaymenthistory');
            $model ->setCustomerId($customer)
                   ->setOrderId($order->getIncrementId())
                   ->setGrandTotal($order->getGrandTotal())
                   ->setState($status)
                   ->setCurrencyType($currencyName)
                   ->save();
        }
       
    }
    
    /**
     * 
     * create invoice with payment
     */
    public function createInvoice(Varien_Event_Observer $observer)
    {
          $order = $observer->getEvent()->getOrder();
          
        if (!$order->getId()) {
            return false;
        }
        //retrieve order invoice availability
        if (!$order->canInvoice()) {
            return false;
        }
        
        $savedQtys = array();
        //create new invoice 
        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice($savedQtys);       
        
        //get total item quantity (include parent item relation)
        if (!$invoice->getTotalQty()) {
            return false;
         }
        
        //we have set option for capture online. This option depends on payment method. Some payment methods support capture online and some don’t.
        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
        //register invoice
        $invoice->register();
         
        $invoice->getOrder()->setCustomerNoteNotify(false);
        $invoice->getOrder()->setIsInProcess(true);
        
        //adding object for using in transaction
        $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
         
        $transactionSave->save();    
    }
    
    /**
     * refund balanse if the customer uses mypayment method
     * @param type $observer
     */
    public function refundBalance($observer){
        //get order from event
        $order = $observer->getEvent()->getCreditmemo()
                ->getOrder();
        
        //get total refunded
        $totalRefunded = $observer->getEvent()->getCreditmemo()
                ->getOrder()
                ->getTotalRefunded();
         
        //get customer id
        $customer = $observer->getEvent()->getPayment()
                ->getOrder()
                ->getCustomerId();
        
        //get currency type
        $currencyName = Mage::app()->getLocale()->currency($order->getOrderCurrencyCode())->getShortName();
        
        //check if payment method is mypayment
        if ($observer ->getEvent()->getPayment()->getMethodInstance()->getCode() == 'mypayment'){ 
            
            //get model object
            $model = Mage::getModel('customer/customer')->load($customer);
            $creditBalans= $model->getCreditBalans();
            $updateBalans = $creditBalans + $totalRefunded;
            
            //update credit balanse
            $model->setCreditBalans($updateBalans);

            $model->save();
            
            //add data to mypayment_history
            $mypaymentModel = Mage::getModel('mypayment/mypaymenthistory');
            $mypaymentModel ->setCustomerId($customer)
                            ->setOrderId($order->getIncrementId())
                            ->setGrandTotal($totalRefunded)
                            ->setState($observer->getEvent()->getCreditmemo()->getStateName())
                            ->setCurrencyType($currencyName)
                            ->save();
        }
    }

    /**
     * add new tad to customers block if the customer has a 'mypayment_history' 
     * 
     */
    public function addTabToCustomerPage($observer)
    {
        $block = $observer->getBlock();
        if ($block->getNameInLayout() !== self::CUSTOMER_TABS_BLOCK_NAME) {
            return;
        }

        $currentCustomer = Mage::registry('current_customer')->getId();
        $orders = Mage::getModel('mypayment/mypaymenthistory')->getCollection();
        $orders->addFieldToFilter('customer_id', $currentCustomer);
      
        if ($orders->getSize()) {
            
            $block->addTabAfter('mypayment_history', array(
                'label'     => Mage::helper('customer')->__('Payment History'),
                'class'     => 'ajax',
                'url'       => Mage::getUrl('*/mypayment/historygrid', array('_current' => true)),
                ), 
                'orders'
            );
        }
    }
}
