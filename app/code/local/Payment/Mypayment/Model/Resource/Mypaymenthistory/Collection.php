<?php
class Payment_Mypayment_Model_Resource_Mypaymenthistory_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Define resource model
     *
     */
    protected function _construct()
    {
        $this->_init('mypayment/mypaymenthistory');
    }
  
   /**
    * Add customer filter
    * 
    * @param type $customerId
    * @return Payment_Mypayment_Model_Resource_Mypaymenthistory_Collection
    */
    public function addCustomerFilter($customerId)
    {
        $this->getSelect()
            ->where('main_table.customer_id = ?', $customerId);
        return $this;
    }

    
}
