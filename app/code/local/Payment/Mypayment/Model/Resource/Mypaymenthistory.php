<?php
class Payment_Mypayment_Model_Resource_Mypaymenthistory extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('mypayment/mypayment_history', 'id');
    }
    
    /**
     * 
     * formating and adding date to mypayment_history
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (!$object->getId()){
            $object->setDate(Mage::getSingleton('core/date')->gmtDate());
        }
        return $this;
    }
}
