<?php


class Payment_Mypayment_Adminhtml_MypaymentController extends Mage_Adminhtml_Controller_Action
{
    /**
    * action to display grid
    *
    * produce and return block's html output
    */
    public function historygridAction()
    {
        $this->getResponse()
            ->setBody($this->getLayout()
            ->createBlock('mypayment/adminhtml_grid')
           
            ->toHtml()
        );
    }
  
    
}
