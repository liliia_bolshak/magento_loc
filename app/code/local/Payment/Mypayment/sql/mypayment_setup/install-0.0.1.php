<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);
/**
 * Add attributes to the customer table
 */
$setup->addAttribute('customer', 'credit_balans', array(
    'type'          => 'decimal',
    'label'         => 'Balans',
    'visible'       => 1,
    'required'      => 1,
    'default'       => 100,
));

$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'credit_balans',
    '100'
);

$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'credit_balans');
$oAttribute->setData('used_in_forms', array('adminhtml_customer')); 
$oAttribute->save();

$installer->endSetup();
