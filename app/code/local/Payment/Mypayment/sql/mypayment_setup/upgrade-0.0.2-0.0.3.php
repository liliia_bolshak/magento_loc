<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'mypayment/mypayment_history'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('mypayment/mypayment_history'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
           'nullable'  => true,
           'identity'  => true,
           'primary'   => true,
        ), 'id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
           'nullable'  => true,
        ), 'Customer Id')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
           'nullable'  => true,
          
        ), 'Order Id')
    ->addColumn('grand_total', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,2', array(
        
        ), 'Grand Total')
    ->addColumn('date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'date')
    ->addColumn('currency_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 3, array(
       
        ), 'Type of currency')
    ->addColumn('state', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        ), 'State')
    ;
$installer->getConnection()->createTable($table);


$installer->endSetup();
