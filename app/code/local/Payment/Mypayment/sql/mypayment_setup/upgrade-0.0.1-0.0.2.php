<?php

$installer = $this;

$installer->startSetup();

$connect = $installer->getConnection();
$connect->addColumn($installer->getTable('sales/quote_payment'), 'comments', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'comments',
    ));
$connect->addColumn($installer->getTable('sales/order_payment'), 'comments', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'comments',
    ));

$installer->endSetup();
