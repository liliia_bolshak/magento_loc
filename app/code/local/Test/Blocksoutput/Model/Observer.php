<?php

class Test_Blocksoutput_Model_Observer
{
    //create clon block, set child, rewrite template
    public function setCustomBlock(Varien_Event_Observer $observer)
    {
        /** @var $_block Mage_Core_Block_Abstract */
        /*Get block instance*/
        $_block = $observer->getBlock();
        /*get Block type*/
        $_type = $_block->getType();
       // var_dump($_type);
       /*Check block type*/
        if ($_type == 'catalog/product_list_related') {
            /*Clone block instance*/
            $_child = clone $_block;
            /*set another type for block*/
            $_child->setType('blocksoutput/simple');
           
            /*set child for block*/
            $_block->setChild('child', $_child);
            
            /*set our template*/
            $_block->setTemplate('blocksoutput/test.phtml');
            
        }
      
    }
    // create instance layout, create block, set child
    public function insertChildBlock($observer)
    {
        /** @var $_block Mage_Core_Block_Abstract */
        /*Get block instance*/
        $_block = $observer->getBlock();
        /*get Block type*/
        $name = $_block->getNameInLayout();
        
        $layout = Mage::getSingleton('core/layout');
       /*Check block type*/
        if ($name == 'catalog.product.related') {
            
            /*Clone block instance*/
            $_child = $layout->createBlock('blocksoutput/simple');

            /*set child for block*/
            $_block->setChild('child2', $_child);
    
            // var_dump(get_class($_block));
            
            /*set our template*/
            $_block->setTemplate('blocksoutput/test.phtml');
            //$_child->setTemplate('blocksoutput/test.phtml');
           // var_dump($_block->getTemplate());
        }
    }
    
    public function addToParentGroup($observer)
    {
        /*Get block instance*/
        $_block = $observer->getBlock();
        $name = $_block->getNameInLayout();
        
        $layout = Mage::getSingleton('core/layout');
       /*Check block type*/
        if ($name == 'product.info') {
            
            /*Clone block instance*/
            $_child = $layout->createBlock('blocksoutput/simple')->setTitle('Test');

            /*set child for block*/
            $_block->append($_child,'child2');
    
            $_child->addToParentGroup('detailed_info');
        }
        
    }
    
    public function addTabToOrder($observer)
    {
//        /*Get block instance*/
//        $block = $observer->getBlock();
//        $name = $block->getNameInLayout();
//        
//        $layout = Mage::getSingleton('core/layout');
//        /*Check block type*/
//        if ($name == 'sales.order.view') {
//            
//            $childItems = $block->getChild('order_items');
//            $block->addToChildGroup('order_info', $childItems);
//             
//            $childInvoices = $layout->createBlock('sales/order_invoice');
//            $childInvoices->setTitle('Invoices');
//            /*set child for block*/
//            $block->append($childInvoices,'childInvoices');
//            
//           
//            $childInvoices->addToParentGroup('order_info');
 //       }
    }

    public function scheduledSend($schedule)
    {
        
        //this collection get all users which have group_id = 4
     	$customer = Mage::getModel("customer/customer")->getCollection();
    	$customer->addFieldToFilter('group_id', '4');
    	$customer->addNameToSelect();
    	$items = $customer->getItems();


    	foreach($items as $item)
    	{
      
        //load email template
       
            $transactionalEmail = Mage::getModel('core/email_template');
            $templateId = $transactionalEmail->loadDefault('send_email_template');

            /* retrieve mail object instance
            */
            $transactionalEmail->getMail();

            //send mail
            $transactionalEmail->sendTransactional(
                            $templateId, 
                            Array('name' => 'admin', 'email' => 'me@example.com'),       
                     $item->getEmail(),
                     $item->getFirstname(),
                            null,
                            null);
        }
           return $this;
    }
}
