<?php


class Test_Blocksoutput_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $xml = $this->loadLayout()->getLayout()->getUpdate()->asString();
        $this->getResponse()->setHeader('Content-type', 'text/plain')->setBody($xml);
        
//        chenge title in Head block
//        $this->loadLayout()->getLayout()->getBlock('head')->setTitle('Hello Magento');
//        $this->renderLayout();
    }

    public function sortAction()
    {
        $block = $this->loadLayout()->getLayout()->getBlock('content');
        $blockA = $this->getLayout()->createBlock('core/template','block.A')->setTemplate('blocksoutput/image.phtml')->setIsAnonymous(false);
        $blockB = $this->getLayout()->createBlock('core/template')->setTemplate('blocksoutput/test.phtml')->setNameInLayout('block.B')->setIsAnonymous(false);
        $blockC = $this->getLayout()->createBlock('core/template')->setTemplate('blocksoutput/render.phtml');
        $sib = $blockA->getNameInLayout();
        $block->insert($blockB)->insert($blockA)->insert($blockC,$sib,true,'');
   
        $this->renderLayout();
        //var_dump($block->getChild());
        
    }

    public function blockAction()
    {
        $this->getResponse()->setBody('Hello Magento!');
    }
    
    public function overrideAction()
    {
        $block = $this->getLayout()->createBlock('blocksoutput/simple')->toHtml();
        $this->getResponse()->setBody($block);
    }
    
     public function templateAction()
    {
        $block = $this->getLayout()->createBlock('core/template')->setTemplate('blocksoutput/render.phtml')->toHtml();
        $this->getResponse()->setBody($block);
    }
    
     public function registryAction()
    {
         Mage::register('key', 'some value');
         
        $block = $this->getLayout()
                ->createBlock('blocksoutput/register')
                ->setTemplate('blocksoutput/register.phtml')
                ->toHtml();
        $this->getResponse()->setBody($block);
    }
    
    public function listBlockAction()
    {
        $tlb = $this->getLayout()->createBlock('core/text_list');
        
        $blockA = $this->getLayout()->createBlock('core/text')->setText('textA');
        
        $blockB = $this->getLayout()->createBlock('core/text')->setText('textB');
        
        $tlb->insert($blockA)->insert($blockB);
        
        //$this->getResponse()->setBody($tlb->toHtml());
        
        $this->loadLayout()
             ->getLayout()
             ->getBlock('content')
             ->insert($tlb);
        
        $this->renderLayout();
             
            
    }
    
    public function somethingAction()
    {
         $tlb = $this->getLayout()->createBlock('core/text_list');
        
        $blockA = $this->getLayout()->createBlock('core/text')->setText('textA');
        
        $blockB = $this->getLayout()->createBlock('core/text')->setText('textB');
        
        $tlb->setChild('new_child',$blockA);
        echo $tlb->getChildHtml('new_child');
        
        
        $this->getResponse()->setBody($tlb->toHtml());
    }
    
    public function sendAction(){
        
        //this collection get all users which have group_id = 4
     	$customer = Mage::getModel("customer/customer")->getCollection();
    	$customer->addFieldToFilter('group_id', '4');
    	$customer->addNameToSelect();
    	$items = $customer->getItems();

        try{
    	foreach($items as $item)
    	{
      
        //load email template
       
            $transactionalEmail = Mage::getModel('core/email_template');
            $templateId = $transactionalEmail->loadDefault('send_email_template');

            /* retrieve mail object instance
            */
            $transactionalEmail->getMail();

            //send mail
            $transactionalEmail->sendTransactional(
                            $templateId, 
                            Array('name' => 'admin', 'email' => 'me@example.com'),       
                     $item->getEmail(),
                     $item->getFirstname(),
                            null,
                            null);
           }
            if (!$transactionalEmail->getSentSuccess()) {

                    throw new Exception();
                }


                //set success message for front page
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('blocksoutput')->__('Your file was sended successfully and we will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirect('*/*/');

                return;
             } catch (Exception $e) {


                //set error message for front page :
                Mage::getSingleton('core/session')->addError(Mage::helper('blocksoutput')->__('Unable to submit your request. Please, try again later'));

                $this->_redirect('*/*/');
                return;
            }
    	
    }
}
