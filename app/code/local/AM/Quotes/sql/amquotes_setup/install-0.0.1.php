<?php

$installer = $this;
$tableQuotes = $installer->getTable('amquotes/table_quotes');

$installer->startSetup();

$installer->getConnection()->dropTable($tableQuotes);
$table = $installer->getConnection()
    ->newTable($tableQuotes)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
    ))
    ->addColumn('thesis', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('created', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        'nullable'  => false,
    ))
    ->addColumn('magazine', Varien_Db_Ddl_Table::TYPE_TEXT, '255', array(
        'nullable'  => false,
    ))
    ->addColumn('link', Varien_Db_Ddl_Table::TYPE_TEXT, '255', array(
        'nullable'  => false,
    ))
    ->addIndex($installer->getIdxName($tableQuotes, array('product_id')),
    array('product_id'))
    ->addForeignKey($installer->getFkName($tableQuotes, 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

$installer->getConnection()->createTable($table);

$tableStore = $installer->getTable('amquotes/quotes_store');

$installer->getConnection()->dropTable($tableStore);
$table = $installer->getConnection()
    ->newTable($tableStore)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'primary'   => true,
    ), 'Quote ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Store ID')
    ->addIndex($installer->getIdxName('amquotes/quotes_store', array('store_id')),
        array('store_id'))
    ->addForeignKey($installer->getFkName('amquotes/quotes_store', 'id', 'amquotes/quotes_store', 'store_id'),
        'id', $installer->getTable('amquotes/table_quotes'), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('amquotes/quotes_store', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

$installer->getConnection()->createTable($table);

$installer->endSetup();