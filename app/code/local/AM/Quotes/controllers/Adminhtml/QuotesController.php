<?php

class AM_Quotes_Adminhtml_QuotesController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('amquotes');

        if ($this->getRequest()->getParam('ajax')) {
            return $this->_forward('quotesGrid');
        }
        $this->_addContent($this->getLayout()->createBlock('adminhtml/template')->setTemplate('am_quotes/popup.phtml'));
        $this->_addContent($this->getLayout()->createBlock('amquotes/adminhtml_quotes'));
        $this->renderLayout();

    }

    /*Function for add New quote button*/
    public function newAction()
    {
        $this->_title($this->__('Add new quote'));
        $this->_title($this->__('New Quote'));

        $model = Mage::getModel('amquotes/quotes');
        Mage::register('current_quotes', $model);

        $this->loadLayout();
        $this->_setActiveMenu('amquotes');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        //Add two blocks fo add new Quote
        $this->_addContent($this->getLayout()->createBlock('amquotes/adminhtml_quotes_add'));
        $this->_addContent($this->getLayout()->createBlock('amquotes/adminhtml_quotes_product_grid'));

        $this->renderLayout();
    }

    /*Function for edit quote */
    public function editAction()
    {
        /*Function for edit quote*/
        $id = (int)$this->getRequest()->getParam('id');

        $model = Mage::getModel('amquotes/quotes');

        if ($data = Mage::getSingleton('adminhtml/session')->getFormData()) {
            $model->setData($data)->setId($id);
        } else {
            $model->load($id);
        }
        Mage::register('current_quotes', $model);

        $this->_title($this->__('Edit quote'));

        $this->loadLayout();
        $this->_setActiveMenu('amquotes');

        $this->_addContent($this->getLayout()->createBlock('amquotes/adminhtml_quotes_edit'));

        $this->renderLayout();

    }

    public function editPopupAction()
    {
        $id = (int)$this->getRequest()->getParam('id');
        $model = Mage::getModel('amquotes/quotes');

        if ($data = Mage::getSingleton('adminhtml/session')->getFormData()) {
            $model->setData($data)->setId($id);
        } else {
            $model->load($id);
        }
        Mage::register('current_quotes', $model);

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('amquotes/adminhtml_quotes_popup'));
        $this->renderLayout();
    }

    public function editCloseAction()
    {
        $id = (int)$this->getRequest()->getParam('id');
        $model = Mage::getModel('amquotes/quotes');

        if ($data = Mage::getSingleton('adminhtml/session')->getFormData()) {
            $model->setData($data)->setId($id);
        } else {
            $model->load($id);
        }
        Mage::register('current_quotes', $model);

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('amquotes/adminhtml_quotes_close'));
        $this->renderLayout();
    }

    /*Function for delete quote*/
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                Mage::getModel('amquotes/quotes')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Quotes \'#%s\' was deleted successfully', $id));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

    /*Mass delete function for grid*/
    public function massDeleteAction()
    {
        $quotes = $this->getRequest()->getParam('quotes', null);

        if (is_array($quotes) && sizeof($quotes) > 0) {
            try {
                foreach ($quotes as $id) {
                    Mage::getModel('amquotes/quotes')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d quotes have been deleted', sizeof($quotes)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select quotes'));
        }
        $this->_redirect('*/*');
    }

    /*Function for save button in grid*/
    public function saveAction()
    {
        $id = $this->getRequest()->getParam('id');
        //Save data to db
        if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('amquotes/quotes');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));

                if (!$model->getCreated()) {
                    $model->setCreated(now());
                }
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The quote \'#%s\' had been saved successfully', $id));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                // Save or Save and Continue
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                } elseif ($this->getRequest()->getParam('close')) {
                    $this->_forward('editClose');
                } else {
                    $this->_redirect('*/*/');
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');

    }

    public function productGridAction()
    {
        $this->getResponse()->setBody($this->getLayout()->createBlock('amquotes/adminhtml_quotes_product_grid')->toHtml());
    }

    /*quotesGrid in the indexAction*/
    public function quotesGridAction()
    {
        $this->getResponse()->setBody($this->getLayout()->createBlock('amquotes/adminhtml_quotes_grid')->toHtml());
    }

    /*Action for show product info in form to add a new quote*/
    public function jsonProductInfoAction()
    {
        $response = new Varien_Object();
        $id = $this->getRequest()->getParam('id');

        if (intval($id) > 0) {
            $product = Mage::getModel('catalog/product')
                ->load($id);

            $response->setId($id);
            $response->addData($product->getData());
            $response->setError(0);
        } else {
            $response->setError(1);
            $response->setMessage(Mage::helper('catalog')->__('Unable to get the product ID.'));
        }
        $this->getResponse()->setBody($response->toJSON());
    }

    /**
     * Initialize product from request parameters
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _initProduct()
    {
        $this->_title($this->__('Catalog'))
            ->_title($this->__('Manage Products'));

        $productId = (int)$this->getRequest()->getParam('id');
        $product = Mage::getModel('catalog/product')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if (!$productId) {
            if ($setId = (int)$this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }

            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }
        }

        $product->setData('_edit_mode', true);
        if ($productId) {
            try {
                $product->load($productId);
            } catch (Exception $e) {
                $product->setTypeId(Mage_Catalog_Model_Product_Type::DEFAULT_TYPE);
                Mage::logException($e);
            }
        }

        $attributes = $this->getRequest()->getParam('attributes');
        if ($attributes && $product->isConfigurable() &&
            (!$productId || !$product->getTypeInstance()->getUsedProductAttributeIds())
        ) {
            $product->getTypeInstance()->setUsedProductAttributeIds(
                explode(",", base64_decode(urldecode($attributes)))
            );
        }

        if ($this->getRequest()->getParam('popup')
            && $requiredAttributes = $this->getRequest()->getParam('required')
        ) {
            $requiredAttributes = explode(",", $requiredAttributes);
            foreach ($product->getAttributes() as $attribute) {
                if (in_array($attribute->getId(), $requiredAttributes)) {
                    $attribute->setIsRequired(1);
                }
            }
        }

        if ($this->getRequest()->getParam('popup')
            && $this->getRequest()->getParam('product')
            && !is_array($this->getRequest()->getParam('product'))
            && $this->getRequest()->getParam('id', false) === false
        ) {

            $configProduct = Mage::getModel('catalog/product')
                ->setStoreId(0)
                ->load($this->getRequest()->getParam('product'))
                ->setTypeId($this->getRequest()->getParam('type'));

            /* @var $configProduct Mage_Catalog_Model_Product */
            $data = array();
            foreach ($configProduct->getTypeInstance()->getEditableAttributes() as $attribute) {

                /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
                if (!$attribute->getIsUnique()
                    && $attribute->getFrontend()->getInputType() != 'gallery'
                    && $attribute->getAttributeCode() != 'required_options'
                    && $attribute->getAttributeCode() != 'has_options'
                    && $attribute->getAttributeCode() != $configProduct->getIdFieldName()
                ) {
                    $data[$attribute->getAttributeCode()] = $configProduct->getData($attribute->getAttributeCode());
                }
            }

            $product->addData($data)
                ->setWebsiteIds($configProduct->getWebsiteIds());
        }

        Mage::register('product', $product);
        Mage::register('current_product', $product);
        Mage::getSingleton('cms/wysiwyg_config')->setStoreId($this->getRequest()->getParam('store'));
        return $product;
    }

    public function postAction()
    {
        if (!$this->_validateFormKey()) {
            // returns to the product item page
            $this->_redirectReferer();
            return;
        }
        $data = $this->getRequest()->getPost();

        if (($product = $this->_initProduct()) && !empty($data)) {
            try {
            $session = Mage::getSingleton('core/session');
            $quotes = Mage::getModel('amquotes/quotes')->setData($data);
                $quotes->setEntityId($quotes->getEntityIdByCode(Mage_Review_Model_Review::ENTITY_PRODUCT_CODE))
                    ->setEntityPkValue($product->getId())
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The quote had been saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');

            } catch (Exception $e) {
                $session->setFormData($data);
                $session->addError($this->__('Unable to post the quote.'));
            }
        }
        $this->_redirect('*/*/');
    }

    public function displayAction()
    {
        /*Get prodcut information*/
        $this->_initProduct();

        $this->getResponse()->setBody($this->getLayout()
            ->createBlock('amquotes/adminhtml_quotes_product_edit_grid')
            ->setProductId(Mage::registry('current_product')->getId())
            ->setUseAjax(true)
            ->toHtml());
    }
}