<?php

class AM_Quotes_Block_Adminhtml_Quotes_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareLayout()
    {
        //Enable wysiwyg
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        }
        parent::_prepareLayout();
    }

    protected function _prepareForm()
    {
        $quote = Mage::registry('current_quotes');

        $product = Mage::getModel('catalog/product')->load($quote->getProductId());
        $helper = Mage::helper('amquotes');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $this->setForm($form);

        $fieldset = $form->addFieldset('edit_form', array('legend' => $helper->__('Quotes Information')));

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();

        if ($quote->getId()) {
            $fieldset->addField('id', 'text', array(
                'label' => $helper->__('Id'),
                'required' => true,
                'name' => 'id',
            ));
        }

        $fieldset->addField('product_name', 'note', array(
            'label' => Mage::helper('amquotes')->__('Product'),
            'text' => '<a href="' . $this->getUrl('*/catalog_product/edit', array('id' => $product->getId())) . '" onclick="this.target=\'blank\'">' . $product->getName() . '</a>'
        ));

        $fieldset->addField('product_id', 'note', array(
            'label' => $helper->__('Product ID'),
            'required' => false,
            'name' => 'product_id',
            'text' => $quote->getProductId()
        ));

        /* Check is single store mode */
        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_id', 'multiselect', array(
                'name' => 'stores[]',
                'label' => $helper->__('Store View'),
                'title' => $helper->__('Store View'),
                'required' => true,
                'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        } else {
            $fieldset->addField('store_id', 'hidden', array(
                'name' => 'stores[]',
                'value' => Mage::app()->getStore(true)->getId()
            ));
            $quote->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('thesis', 'editor', array(
            'name' => 'thesis',
            'label' => $helper->__('Thesis'),
            'maxlength' => '250',
            'required' => true,
            'config' => $wysiwygConfig
        ));

        $fieldset->addField('magazine', 'text', array(
            'name' => 'magazine',
            'label' => $helper->__('Magazine'),
            'maxlength' => '250',
            'required' => true,
        ));

        $fieldset->addField('link', 'text', array(
            'name' => 'link',
            'label' => $helper->__('Link'),
            'maxlength' => '250',
            'required' => true,
        ));

        $fieldset->addField('created', 'date', array(
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'label' => $helper->__('Created'),
            'required' => true,
            'name' => 'created'
        ));

        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('*/*/save'));
        $form->setValues($quote->getData());

        return parent::_prepareForm();
    }
}