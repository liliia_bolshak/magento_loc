<?php

class AM_Quotes_Block_Adminhtml_Quotes_Popup extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'amquotes';
        $this->_controller = 'adminhtml_quotes';

        //ButtonSave And Close in edit tab
        $this->_addButton('save_and_cclose', array(
            'label' => Mage::helper('adminhtml')->__('Save And Close'),
            'onclick' => 'saveAndClose()',
            'class' => 'save',
        ), -100);
        $this->_updateButton('save', 'label', Mage::helper('amquotes')->__('Save Example'));

        $this->_formScripts[] = "
            function saveAndClose(){
                editForm.submit($('edit_form').action+'close/editClose');
            }
        ";

    }

    public function __construct()
    {
        parent::__construct();
        $this->_removeButton('delete');
        $this->_removeButton('reset');
        $this->_removeButton('back');
        $this->_removeButton('save');

        $this->_addButton('button_id', array(
            'label'     => Mage::helper('amquotes')->__('Close Window'),
            'onclick'   => "top.oPopup.close()",
            'class'     => 'cancel',
        ));
    }
}
