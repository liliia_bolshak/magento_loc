<?php

class AM_Quotes_Block_Adminhtml_Quotes_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _construct()
    {
        $this->setId('quotesGrid');
        $this->_controller = 'adminhtml_quotes';
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('amquotes/quotes')
            ->getCollection()
            ->setFirstStoreFlag(true);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('amquotes');

        $this->addColumn('id', array(
            'header' => $helper->__('Quote ID'),
            'index' => 'id',
            'width' => '50px',
            'align' => 'center'
        ));

        $this->addColumn('thesis', array(
            'header' => $helper->__('Title'),
            'index' => 'thesis',
            'type' => 'text',
            'width' => '500px',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header' => $helper->__('Store View'),
                'index' => 'store_id',
                'type' => 'store',
                'width' => '150px',
                'align' => 'center',
                'store_all' => true,
                'store_view' => true,
                'sortable' => false,
                'filter_condition_callback'
                => array($this, '_filterStoreCondition'),
            ));
        }

        $this->addColumn('product_id', array(
            'header' => $helper->__('Product Id'),
            'index' => 'product_id',
            'type' => 'text',
            'width' => '60px',
            'align' => 'center',
        ));

        $this->addColumn('Magazine', array(
            'header' => $helper->__('Magazine'),
            'index' => 'magazine',
            'type' => 'text',
        ));

        $this->addColumn('link', array(
            'header' => $helper->__('Link'),
            'index' => 'link',
            'type' => 'text',
        ));

        $this->addColumn('created', array(
            'header' => $helper->__('Created on'),
            'index' => 'created',
            'type' => 'date',
        ));

        return parent::_prepareColumns();
    }

    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $this->getCollection()->addStoreFilter($value);
    }

    protected function _prepareMassaction()
    {   /*Mass delete function*/
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('quotes');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => 'Are you sure?'
        ));
        return $this;
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $model->getId(),
            'productId' => $model->getProductId(),
        ));

    }

}