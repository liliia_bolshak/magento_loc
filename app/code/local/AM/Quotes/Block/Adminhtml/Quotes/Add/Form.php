<?php

class AM_Quotes_Block_Adminhtml_Quotes_Add_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareLayout() {
        //Enable wysiwyg
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        }
        parent::_prepareLayout();
    }

    protected function _prepareForm()
    {
        $quote = Mage::registry('current_quotes');
        $helper = Mage::helper('amquotes');

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();

        $form = new Varien_Data_Form(array(
            'id' => 'add_amquotes_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $fieldset = $form->addFieldset('add_amquotes_form', array('legend' => Mage::helper('amquotes')->__('Quotes Details')));

        $fieldset->addField('product_name', 'note', array(
            'label'     => Mage::helper('amquotes')->__('Product'),
            'text'      => 'product_name',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_id', 'multiselect', array(
                'label'     => $helper->__('Store View'),
                'required'  => true,
                'name'      => 'stores[]',
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }

        $fieldset->addField('thesis', 'editor', array(
            'name'      => 'thesis',
            'title'     => Mage::helper('amquotes')->__('Summary of Review'),
            'label'     => Mage::helper('amquotes')->__('Summary of Review'),
            'maxlength' => '255',
            'required'  => true,

            'config'    =>  $wysiwygConfig,
        ));

        $fieldset->addField('magazine', 'text', array(
            'name'      => 'magazine',
            'label'     => $helper->__('Magazine'),
            'maxlength' => '250',
            'required'  => true,
        ));

        $fieldset->addField('link', 'text', array(
            'name'      => 'link',
            'title'     => Mage::helper('amquotes')->__('Link'),
            'label'     => Mage::helper('amquotes')->__('Link'),
            'maxlength' => '250',
            'required'  => true,
        ));

        $fieldset->addField('created', 'date', array(
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'title'     => Mage::helper('amquotes')->__('Created'),
            'label'     => Mage::helper('amquotes')->__('Created'),
            'required' => true,
            'name' => 'created'
        ));

        $fieldset->addField('product_id', 'hidden', array(
            'name'      => 'product_id',
        ));

        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('*/*/post'));

        $this->setForm($form);
    }
}
