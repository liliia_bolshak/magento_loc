<?php

class AM_Quotes_Block_Adminhtml_Quotes_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'amquotes';
        $this->_controller = 'adminhtml_quotes';
        $this->_mode = 'edit';

        //ButtonSave And Continue in edit tab
        $this->_addButton('save_and_continue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);
        $this->_updateButton('save', 'label', Mage::helper('amquotes')->__('Save Example'));

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('form_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'edit_form');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'edit_form');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

    }

    public function getHeaderText()
    {
        //Buttons for add new quote and when active Edit look for quote id and receive Edit Quotes item (quote id)
        $helper = Mage::helper('amquotes');
        $model = Mage::registry('current_quotes');

        if ($model->getId()) {
            return $helper->__("Edit Quotes item '%s'", $this->escapeHtml($model->getId()));
        } else {
            return $helper->__("Add Quotes item");
        }
    }
}
