<?php

class AM_Quotes_Block_Adminhtml_Quotes_Close extends Mage_Adminhtml_Block_Widget_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_addButton('close', array(
            'label'     => Mage::helper('amquotes')->__('Close Window'),
            'onclick'   => "top.oPopup.close()",
            'class'     => 'cancel',
            'id'        => 'button_close',
        ));

        $this->setTemplate('widget/view/container.phtml');
        $this->_headerText = $this->__('Please click on the Close Window button if it is not closed automatically.');
    }

    public function getViewHtml()
    {
        return '';
    }
}
