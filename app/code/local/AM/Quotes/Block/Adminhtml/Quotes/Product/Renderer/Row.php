<?php
class AM_Quotes_Block_Adminhtml_Quotes_Product_Renderer_Row extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        return  '<a href="#" onclick=popupWindow(\''.$this->getUrl('*/*/editPopup', array('id' => $row->getId())).'\')>Edit</a>';
    }

}
