<?php

class AM_Quotes_Block_Adminhtml_Quotes_Product_Edit_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('productGrid');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('amquotes/quotes')
            ->getCollection()
            ->addFieldToFilter('product_id', $this->getProductId());
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('amquotes');

        $this->addColumn('id', array(
            'header'        => $helper->__('Quote Id'),
            'align'         => 'right',
            'width'         => '50px',
            'index'         => 'id',
            'filter_index'  => 'qt.id',
        ));

        $this->addColumn('thesis', array(
            'header' => $helper->__('Thesis'),
            'index'  => 'thesis',
            'type'   => 'text',
            'filter_index'  => 'qt.thesis',
        ));

        $this->addColumn('link', array(
            'header' => $helper->__('Link'),
            'index' => 'link',
            'type' => 'link',
            'filter_index'  => 'qt.link',
        ));

        $this->addColumn('created', array(
            'header' => $helper->__('Created on'),
            'index' => 'created',
            'type' => 'created',
            'filter_index'  => 'qt.created',
        ));

        $this->addColumn('action', array(
            'header'    =>  Mage::helper('amquotes')->__('Action'),
            'renderer'  => 'AM_Quotes_Block_Adminhtml_Quotes_Product_Renderer_Row',
        ));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        return $this;
    }


}


