<?php

class AM_Quotes_Block_Adminhtml_Quotes extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();

        $helper = Mage::helper('amquotes');
        $this->_blockGroup = 'amquotes';
        $this->_controller = 'adminhtml_quotes';

        $this->_headerText = $helper->__('Quote Management');
        $this->_addButtonLabel = $helper->__('Add Quotes');
    }


}