<?php

class AM_Quotes_Block_Product_View extends Mage_Catalog_Block_Product_View
{
    protected $_quotesCollection;

    public function getQuotesCollection()
    {
        if (null === $this->_quotesCollection) {
            $currentStore = Mage::app()->getStore()->getStoreId();
            $this->_quotesCollection = Mage::getModel('amquotes/quotes')->getProductCollection()
                ->addStoreFilter($currentStore)
                ->addEntityFilter($this->getProduct()->getId());
        }

        return $this->_quotesCollection;
    }



}
