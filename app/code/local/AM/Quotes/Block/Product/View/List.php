<?php

class AM_Quotes_Block_Product_View_List extends Mage_Review_Block_Product_View
{

    public function getProductId()
    {
        return Mage::registry('product')->getId();
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        return $this;
    }

}
