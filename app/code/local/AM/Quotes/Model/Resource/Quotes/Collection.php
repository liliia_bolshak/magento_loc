<?php

class AM_Quotes_Model_Resource_Quotes_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected $_quotesTable;
    protected $_quotesStoreTable;

    protected function _construct()
    {
        $this->_init('amquotes/quotes');
        $this->_quotesTable         = $this->getTable('amquotes/table_quotes');
        $this->_quotesStoreTable    = $this->getTable('amquotes/quotes_store');
        parent::_construct();
    }

    /**
     * Set first store flag
     *
     * @param bool $flag
     * @return Mage_Cms_Model_Resource_Page_Collection
     */
    public function setFirstStoreFlag($flag = false)
    {
        $this->_previewFlag = $flag;

        return $this;
    }

    /**
     * Add filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     * @param bool $withAdmin
     * @return Mage_Cms_Model_Resource_Page_Collection
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            if ($store instanceof Mage_Core_Model_Store) {
                $store = array($store->getId());
            }

            if (!is_array($store)) {
                $store = array($store);
            }

            if ($withAdmin) {
                $store[] = Mage_Core_Model_App::ADMIN_STORE_ID;
            }

            $this->addFilter('store_id', array('in' => $store), 'public');
        }
        return $this;
    }


    /**
     * Join store relation table if there is store filter
     */
    protected function _renderFiltersBefore()
    {
        if ($this->getFilter('store_id')) {
            $this->getSelect()->join(
                array('store_table' => $this->getTable('amquotes/quotes_store')),
                'main_table.id = store_table.id',
                array()
            )->group('main_table.id');

            /*
             * Allow analytic functions usage because of one field grouping
             */
            $this->_useAnalyticFunction = true;
        }
        return parent::_renderFiltersBefore();
    }

    public function addEntityFilter($entityId)
    {
        $this->getSelect()
            ->where('qt.product_id = ?', $entityId);
        return $this;
    }

    /**
     * Get SQL for get record count.
     * Extra GROUP BY strip added.
     *
     * @return Varien_Db_Select
     */
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSelect->reset(Zend_Db_Select::GROUP);

        return $countSelect;
    }

    public function addStoreData()
    {
        $adapter = $this->getConnection();
        $quotesIds = $this->getColumnValues('id');

        $storesToQuotes = array();
        if (count($quotesIds) > 0) {
            $quotesIdCondition = $this->_getConditionSql('id', array('in' => $quotesIds));
            $storeIdCondition = $this->_getConditionSql('store_id', array('gt' => 0));
            $select = $adapter->select()
                ->from($this->_quotesStoreTable)
                ->where($quotesIdCondition)
                ->where($storeIdCondition);
            $result = $adapter->fetchAll($select);

            foreach ($result as $row) {
                if (!isset($storesToQuotes[$row['id']])) {
                    $storesToQuotes[$row['id']] = array();
                }
                $storesToQuotes[$row['id']][] = $row['store_id'];
            }
        }
        foreach ($this as $item) {
            if (isset($storesToQuotes[$item->getId()])) {
                $item->setData('stores', $storesToQuotes[$item->getId()]);
            } else {
                $item->setData('stores', array());
            }
        }
    }



}