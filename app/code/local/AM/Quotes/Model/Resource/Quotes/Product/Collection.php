<?php

class AM_Quotes_Model_Resource_Quotes_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection
{
    /**
     * Entities alias
     *
     * @var array
     */
    protected $_entitiesAlias        = array();

    /**
     * Filter by stores for the collection
     *
     * @var array
     */
    protected $_storesIds            = array();

    /**
     * Quotes store table
     *
     * @var string
     */
    protected $_quotesStoreTable;

    /**
     * Add store data flag
     *
     * @var boolean
     */
    protected $_addStoreDataFlag     = false;

    /**
     * Use analytic function flag
     * If true - allows to prepare final select with analytic functions
     *
     * @var bool
     */
    protected $_useAnalyticFunction         = false;
    protected $_quotesTable;

    protected function _construct()
    {
        $this->_init('catalog/product');
        $this->setRowIdFieldName('id');
        $this->_quotesTable         = $this->getTable('amquotes/table_quotes');
        $this->_quotesStoreTable = Mage::getSingleton('core/resource')->getTableName('amquotes/quotes_store');
        $this->_initTables();
    }

    /**
     * Init select
     *
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->_joinFields();
        return $this;
    }

    /**
     * Adds store filter into array
     *
     * @param mixed $storeId
     * @return AM_Quotes_Model_Resource_Quotes_Product_Collection
     */
    public function addStoreFilter($storeId = null)
    {
        if (is_null($storeId)) {
            $storeId = $this->getStoreId();
        }

        parent::addStoreFilter($storeId);

        if (!is_array($storeId)) {
            $storeId = array($storeId);
        }

        if (!empty($this->_storesIds)) {
            $this->_storesIds = array_intersect($this->_storesIds, $storeId);
        } else {
            $this->_storesIds = $storeId;
        }
        return $this;
    }

    /**
     * Adds specific store id into array
     *
     * @param array $storeId
     * @return AM_Quotes_Model_Resource_Quotes_Product_Collection
     */
    public function setStoreFilter($storeId)
    {
        if (is_array($storeId) && isset($storeId['eq'])) {

            $storeId = array_shift($storeId);
        }

        if (!is_array($storeId)){
            $storeId = array($storeId);
        }

        if (!empty($this->_storesIds)){
            $this->_storesIds = array_intersect($this->_storesIds, $storeId);
        } else {
            $this->_storesIds = $storeId;
        }

        return $this;
    }

    /**
     * Applies all store filters in one place to prevent multiple joins in select
     *
     * @param null|Zend_Db_Select $select
     * @return AM_Quotes_Model_Resource_Quotes_Product_Collection
     */
    protected function _applyStoresFilterToSelect(Zend_Db_Select $select = null)
    {
        $adapter = $this->getConnection();
        $storesIds = $this->_storesIds;

        if (is_null($select)){
            $select = $this->getSelect();
        }

        if (is_array($storesIds) && (count($storesIds) == 1)) {
            $storesIds = array_shift($storesIds);
        }

        if (is_array($storesIds) && !empty($storesIds)) {
            $inCond = $adapter->prepareSqlCondition('store.store_id', array('in' => $storesIds));
            $select->join(array('store' => $this->_quotesStoreTable),
                'qt.id=store.id AND ' . $inCond,
                array())
                ->group('qt.id');
            $this->_useAnalyticFunction = true;
        } else {
            $select->join(array('store' => $this->_quotesStoreTable),
                $adapter->quoteInto('qt.id=store.id AND store.store_id = ?', (int)$storesIds),
                array());
        }
        return $this;
    }

    /**
     * Add stores data
     *
     * @return AM_Quotes_Model_Resource_Quotes_Product_Collection
     */
    public function addStoreData()
    {
        $this->_addStoreDataFlag = true;
        return $this;
    }

    public function addEntityFilter($entityId)
    {
        $this->getSelect()
            ->where('qt.product_id = ?', $entityId);
        return $this;
    }

    /**
     * Join fields to entity
     *
     */
    protected function _joinFields()
    {
        $quotesTable = Mage::getSingleton('core/resource')->getTableName('amquotes/table_quotes');

        $this->addAttributeToSelect('name');

        $this->getSelect()
            ->join(array('qt' => $quotesTable),
                'qt.product_id = e.entity_id',
                array('qt.id', 'qt.product_id', 'qt.thesis', 'qt.created', 'qt.magazine', 'qt.link'));
        return $this;
    }

    /**
     * Render SQL for retrieve product count
     *
     * @return string
     */
    public function getSelectCountSql()
    {
        $select = parent::getSelectCountSql();
        $select->reset(Zend_Db_Select::COLUMNS)
            ->columns('COUNT(qt.id)')
            ->reset(Zend_Db_Select::HAVING);

        return $select;
    }

    /**
     * Set order to attribute
     *
     * @param string $attribute
     * @param string $dir
     * @return AM_Quotes_Model_Resource_Quotes_Product_Collection
     */
    public function setOrder($attribute, $dir = 'DESC')
    {
        switch($attribute) {
            case 'qt.id':
            case 'qt.product_id':
            case 'qt.thesis':
            case 'qt.created':
            case 'qt.magazine':
            case 'qt.link':
                $this->getSelect()->order($attribute . ' ' . $dir);
                break;
            case 'stores':
                // No way to sort
                break;
            default:
                parent::setOrder($attribute, $dir);
        }

        return $this;

    }

    /**
     * Add attribute to filter
     *
     * @param Mage_Eav_Model_Entity_Attribute_Abstract|string $attribute
     * @param array $condition
     * @param string $joinType
     * @return AM_Quotes_Model_Resource_Quotes_Product_Collection
     */

    public function addAttributeToFilter($attribute, $condition = null, $joinType = 'inner')
    {
        switch($attribute) {
            case 'qt.id':
            case 'qt.product_id':
            case 'qt.thesis':
            case 'qt.created':
            case 'qt.magazine':
            case 'qt.link':
                $conditionSql = $this->_getConditionSql($attribute, $condition);
                $this->getSelect()->where($conditionSql);
                break;
            case 'stores':
                $this->setStoreFilter($condition);
                break;
            default:
                parent::addAttributeToFilter($attribute, $condition, $joinType);
                break;
        }
        return $this;
    }

    /**
     * Retrieves column values
     *
     * @param string $colName
     * @return array
     */
    public function getColumnValues($colName)
    {
        $col = array();
        foreach ($this->getItems() as $item) {
            $col[] = $item->getData($colName);
        }
        return $col;
    }

    /**
     * Action after load
     *
     * @return AM_Quotes_Model_Resource_Quotes_Product_Collection
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();
        if ($this->_addStoreDataFlag) {
            $this->_addStoreData();
        }
        return $this;
    }

    /**
     * Add store data
     *
     */
    protected function _addStoreData()
    {
        $adapter = $this->getConnection();
        $quotesIds = $this->getColumnValues('id');

        $storesToQuotes = array();
        if (count($quotesIds) > 0) {
            $quotesIdCondition = $this->_getConditionSql('id', array('in' => $quotesIds));
            $storeIdCondition = $this->_getConditionSql('store_id', array('gt' => 0));
            $select = $adapter->select()
                ->from($this->_quotesStoreTable)
                ->where($quotesIdCondition)
                ->where($storeIdCondition);
            $result = $adapter->fetchAll($select);

            foreach ($result as $row) {
                if (!isset($storesToQuotes[$row['id']])) {
                    $storesToQuotes[$row['id']] = array();
                }
                $storesToQuotes[$row['id']][] = $row['store_id'];
            }
        }
        foreach ($this as $item) {
            if (isset($storesToQuotes[$item->getId()])) {
                $item->setData('stores', $storesToQuotes[$item->getId()]);
            } else {
                $item->setData('stores', array());
            }
        }
    }

    /**
     * Redeclare parent method for store filters applying
     *
     * @return AM_Quotes_Model_Resource_Quotes_Product_Collection
     */
    protected function _beforeLoad()
    {
        parent::_beforeLoad();
        $this->_applyStoresFilterToSelect();

        return $this;
    }
}
