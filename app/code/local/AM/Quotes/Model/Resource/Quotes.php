<?php

class AM_Quotes_Model_Resource_Quotes extends Mage_Core_Model_Resource_Db_Abstract
{

    public function _construct()
    {
        $this->_init('amquotes/table_quotes', 'id');

    }
    /**
     * Perform actions before object save
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if ($object->hasData('stores') && is_array($object->getStores())) {
            $stores = $object->getStores();
            $object->setStores($stores);
        } elseif ($object->hasData('stores')) {
            $object->setStores(array($object->getStores()));
        }
        return $this;
    }

    /**
     * Assign page to store views
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Cms_Model_Resource_Page
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $insert = (array)$object->getStores();
        $table  = $this->getTable('amquotes/quotes_store');

        $where = array(
            'id = ?'     => (int) $object->getId(),
        );
        $this->_getWriteAdapter()->delete($table, $where);

        if ($insert) {
            $data = array();

            foreach ($insert as $storeId) {
                $data[] = array(
                    'id'  => (int) $object->getId(),
                    'store_id' => (int) $storeId
                );
            }

            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }

        return parent::_afterSave($object);

    }

    /**
     * Perform operations after object load
     *
     * @param Mage_Core_Model_Abstract $object
     * @return AM_Quotes_Model_Resource_Quotes
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {

        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());

            $object->setData('store_id', $stores);
            $object->setData('stores', $stores);
        }

        return parent::_afterLoad($object);
    }

    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupStoreIds($id)
    {
        $adapter = $this->_getReadAdapter();

        $select  = $adapter->select()
            ->from($this->getTable('amquotes/quotes_store'), 'store_id')
            ->where('id = ?',(int)$id);

        $binds = array(
            ':id' => (int) $id
        );

        return $adapter->fetchCol($select, $binds);
    }


}