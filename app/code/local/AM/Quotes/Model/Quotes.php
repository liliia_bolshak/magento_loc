<?php

class AM_Quotes_Model_Quotes extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('amquotes/quotes');
    }

    public function getProductCollection()
    {
        return Mage::getResourceModel('amquotes/quotes_product_collection');
    }
}