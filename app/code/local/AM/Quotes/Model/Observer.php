<?php

class AM_Quotes_Model_Observer
{
    public function addQuotesTab($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs) {
            if (Mage::app()->getRequest()->getParam('id')) {
                $block->addTab('quotes', array(
                    'label' => Mage::helper('catalog')->__('Quotes'),
                    'url' => Mage::getUrl('*/quotes/display', array('_current' => true)),
                    'class' => 'ajax',
                ));
            }
        }
    }
}