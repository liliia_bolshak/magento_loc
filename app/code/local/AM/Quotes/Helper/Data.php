<?php

class AM_Quotes_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function filterHtml($html)
    {
        /*Create filter for html*/
        $helper = Mage::helper('cms');
        $processor = $helper->getBlockTemplateProcessor();
        return $processor->filter($html);
    }
}
