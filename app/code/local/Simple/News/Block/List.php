<?php

class Simple_News_Block_List extends Mage_Core_Block_Template
{
    protected $_collection;
    
    public function getNewsCollection()
    {
        if (!$this->_collection) {
            $currentStore = Mage::app()->getStore()->getStoreId();

            $newsCollection = Mage::getModel('news/news')->getCollection();
            $newsCollection->addFieldToFilter('is_active',true)
                ->setOrder('news_id', 'DESC')
                ->addStoreFilter($currentStore);

            $this->_collection = $newsCollection;
        }

        return $this->_collection;
    }

    protected function _prepareLayout()
    {
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')
                ->addCrumb('home', array('label'=>Mage::helper('cms')->__('Home'), 'title'=>Mage::helper('cms')->__('Go to Home Page'), 'link'=>Mage::getBaseUrl()))
                ->addCrumb('news list', array('label'=>'News List', 'title'=>'News List'));
       

        return parent::_prepareLayout();
    }
    
    protected function _beforeToHtml()
    {
       //create block pager
        $pager = $this->getLayout()->createBlock('page/html_pager', 'news.pager');
        //set limit on collection
        $collection = $this->getNewsCollection();
        $pager->setAvailableLimit(array(2 => 2, 10=>10));
        // set collection to toolbar
        $pager->setCollection($collection);
        //Set child block      
        $this->setChild('news.pager', $pager);

        return $this;
    }

    /**
     * 
     * Retrieve child block HTML
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('news.pager');
    }
   
}
