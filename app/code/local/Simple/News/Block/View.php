<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Simple_News_Block_View extends Mage_Core_Block_Template
{
    /**
     * Reads news from the registry
     */
    public function getNews()
    {
        $news = Mage::registry('news');
        
        return $news;
    }

    protected function _prepareLayout()
    {
    if($news = $this->getNews()){
        
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')
            ->addCrumb('home', array('label'=>Mage::helper('cms')->__('Home'), 'title'=>Mage::helper('cms')->__('Go to Home Page'), 'link'=>Mage::getBaseUrl()))
            ->addCrumb('news list', array('label'=>'News List', 'title'=>'News List', 'link'=>Mage::getBaseUrl().'/news/index/list'))
            ->addCrumb('news', array('label'=>$news->getTitle(), 'title'=>$news->getTitle()));
    }
// $breadcrumbs = $this->getLayout()->getBlock('news.breadcrumbs')->toHtml();
    
    return parent::_prepareLayout();
    }
    /**
     * Additional registry validation for news presense on render
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->getNews()) {
            
            return parent::_toHtml();
        } else {
            return '';
        }
    }
}
