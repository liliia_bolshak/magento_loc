<?php


class Simple_News_Block_Adminhtml_News_Grid_Renderer_Action
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * 
     * make reference to Preview
     * @return type
     */
    public function render(Varien_Object $row)
    {
       
        $href = Mage::getUrl('news/index/view', array(
                '_current' => true,
                'news_id' => $row->getId()
            )); 
         
        return '<a href="'.$href.'" target="_blank">'.$this->__('Preview').'</a>';
    }
}
