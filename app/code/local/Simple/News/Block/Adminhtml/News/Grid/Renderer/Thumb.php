<?php

class Simple_News_Block_Adminhtml_News_Grid_Renderer_Thumb
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * 
     * make reference to Preview
     * @return type
     */
    public function render(Varien_Object $row)
    {
       
        $helper = Mage::helper('news/image');
        $helper->init($row, 'path_img')
            ->keepAspectRatio(true);
        return sprintf('<img src="%s" />', $helper->resize(100, 75));
    }
}
