<?php


class Simple_News_Block_Adminhtml_News_Edit_Tab_Content
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        /**  */
        $model = Mage::registry('news');

        /*
         * Checking if user have permissions to save information
         */
        
        if ($this->_isAllowedAction('save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }


        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('new_');

        $fieldset = $form->addFieldset('content_fieldset', array('legend'=>Mage::helper('news')->__('Content'),'class'=>'fieldset-wide'));
        $this->_addElementTypes($fieldset);

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
            array('tab_id' => $this->getTabId())
        );

        $fieldset->addField('summary', 'editor', array(
            'name'      => 'summary',
            'label'     => Mage::helper('news')->__('Summary'),
            'title'     => Mage::helper('news')->__('Summary'),
            'disabled'  => $isElementDisabled
        ));

        $contentField = $fieldset->addField('content', 'editor', array(
            'name'      => 'content',
            'label'     => $this->__('Content'),
            'style'     => 'height:36em;',
            'required'  => true,
            'disabled'  => $isElementDisabled,
            'config'    => $wysiwygConfig
        ));
        
        $contentField = $fieldset->addField('path_img', 'image', array(
            'name'      => 'path_img',
            'label'     => $this->__('Image'),
            'disabled'  => $isElementDisabled,
            'config'    => $wysiwygConfig
        ));

        $form->setValues($model->getData());
        $this->setForm($form);

        Mage::dispatchEvent('adminhtml_news_edit_tab_content_prepare_form', array('form' => $form));

        return parent::_prepareForm();
    }
    
    /**
     * Retrieve predefined additional element types
     * @return array
     */
    protected function _getAdditionalElementTypes()
    {
        return array(
            'image' => Mage::getConfig()->getBlockClassName('news/adminhtml_news_form_renderer_image')
        );
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('news')->__('Content');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('news')->__('Content');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('news/' . $action);
    }
}
