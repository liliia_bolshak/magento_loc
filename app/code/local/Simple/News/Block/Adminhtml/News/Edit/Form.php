<?php

class Simple_News_Block_Adminhtml_News_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
//Create a form content block
//This block only defines a form that has no items. The form item will be added by a
//Tabs block
    protected function _prepareForm()
    {
        $news = Mage::registry('news');
        //создаем объект типа Varien_Data_Form
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('block_id' => $news->getId())),
            'method' => 'post',
            'enctype' => 'multipart/form-data',
            ));
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}
