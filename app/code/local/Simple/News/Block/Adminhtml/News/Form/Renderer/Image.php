<?php
/**
*
 */

class Simple_News_Block_Adminhtml_News_Form_Renderer_Image extends Varien_Data_Form_Element_Image
{       
    /**
    * override method to get a link to an image
    */
    protected function _getUrl()
    {
        
        $url = false;
        if ($this->getValue()) {
            $url = Mage::getBaseUrl('media') .'news'. $this->getValue();
        }

        return $url;
    }
}
