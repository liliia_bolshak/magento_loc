<?php

class Simple_News_Block_Adminhtml_News_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        
        parent::__construct();
        $this->setId('newsGrid');
        $this->_controller = 'adminhtml_news';
        $this->setDefaultDir('ASC');//sorting
       // $this->setPagerVisibility(false);
    }

    protected function _prepareCollection()
    {
        // Get and set our collection for the grid
        $collection = Mage::getModel('news/news')->getCollection();
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
         $dateFormatIso = Mage::app()->getLocale() ->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
       // Add the columns that should appear in the grid
        $baseUrl = $this->getUrl();
        $this->addColumn('news_id', array(
            'header'        => Mage::helper('news')->__('ID'),
            'align'         => 'right',
            'width'         => '20px',
            'index'         => 'news_id'
        ));
        $this->addColumn('title', array(
            'header'    => Mage::helper('news')->__('Title'),
            'align'     => 'left',
            'index'     => 'title',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'        => Mage::helper('news')->__('Store View'),
                'index'         => 'store_id',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
            ));
        }
        $this->addColumn('public_time', array(
            'header'    => Mage::helper('news')->__('Time of Publication'),
            'index'     => 'public_time',
            'type'      => 'date',
            'format' => $dateFormatIso,
        ));
        $this->addColumn('is_active', array(
            'header'    => Mage::helper('news')->__('Status'),
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => array(
                0 => Mage::helper('news')->__('Disabled'),
                1 => Mage::helper('news')->__('Enabled')
            ),
        ));

        $this->addColumn('author', array(
            'header'    => Mage::helper('news')->__('Author'),
            'align'     => 'left',
            'index'     => 'author',
        ));
        
        $this->addColumn('path_img', array(
            'header'    => Mage::helper('news')->__('Thumb image'),
            'align'     => 'left',
            'index'     => 'path_img',
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'news/adminhtml_news_grid_renderer_thumb',
        ));
        
        $this->addColumn('summary', array(
            'header'    => Mage::helper('news')->__('Summary'),
            'align'     => 'left',
            'index'     => 'summary',
        ));
        $this->addColumn('page_actions', array(
            'header'    => Mage::helper('news')->__('Action'),
            'width'     => 10,
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'news/adminhtml_news_grid_renderer_action',
        ));

        return parent::_prepareColumns();
    }

    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addStoreFilter($value);
    }

    /**
     * Row click url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
         // This is where our row data will link to
        return $this->getUrl('*/*/edit', array('news_id' => $row->getId()));
    }

}
