<?php


class Simple_News_Block_Adminhtml_News extends Mage_Adminhtml_Block_Widget_Grid_Container
{
//block grid container
    
    public function __construct()
    {        
        $this->_blockGroup = 'news';
        //where is the controller
        $this->_controller = 'adminhtml_news';
        $this->_headerText = $this->__('News');
        $this->_addButtonLabel = $this->__('Add News');
        parent::__construct();
    }

}