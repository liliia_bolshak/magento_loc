<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'news/news'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('news/news'))
    ->addColumn('news_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'News ID')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'News Title')
    ->addColumn('path_img', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
        ), 'News Image')
    ->addColumn('public_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'News Time Publication')
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '1',
        ), 'Is News Active')
    ->addColumn('content', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        ), 'News Content')
    ->addColumn('author', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        ), 'News Author')
    ->setComment('News Table');
$installer->getConnection()->createTable($table);

/**
 * Create table 'news/news_store'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('news/news_store'))
    ->addColumn('news_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'primary'   => true,
        ), 'News ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')
    ->addIndex($installer->getIdxName('news/news_store', array('store_id')),
        array('store_id'))
    ->addForeignKey($installer->getFkName('news/news_store', 'news_id', 'news/news_store', 'store_id'),
        'news_id', $installer->getTable('news/news'), 'news_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('news/news_store', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('CMS News To Store Linkage Table');
$installer->getConnection()->createTable($table);

$installer->endSetup();
