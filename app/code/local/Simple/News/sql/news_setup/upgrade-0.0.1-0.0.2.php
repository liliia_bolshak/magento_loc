<?php

$installer = $this;

$installer->getConnection()
    ->addColumn($installer->getTable('news/news'), 'summary', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Summary',
        'length'  => '255'
    ));
