<?php



class Simple_News_Adminhtml_NewsController extends Mage_Adminhtml_Controller_Action
{
//
//    public function indexAction()
//    {
//        $this->loadLayout(); //инициализация макета страницы админки
//       
//        $this->_setActiveMenu('news'); //выставляем текущий активный пункт меню
//
//        $contentBlock = $this->getLayout()->createBlock('adminhtml/news'); // обращаемся к уже сформированному макету стр.
//                                                                          // создаем блок
//        $this->_addContent($contentBlock);                                //созданный блок добавляется в макет в качестве содержимого страницы.
//        $this->renderLayout();
//    }
//   
    /**
     * 
     *  this method will set some basic params for each action

     */
     protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('news')
            ->_addBreadcrumb(Mage::helper('news')->__('News'), Mage::helper('news')->__('News'))
            
        ;
        return $this;
    }

    /**
     * Index action
     */
    public function indexAction()
    {
       $this->_title($this->__('News'));


        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Create new News page
     */
    public function newAction()
    {
        // the same form is used to create and edit
        $this->_forward('edit');
    }

    /**
     * Edit News
     */
    public function editAction()
    {
        
        $this->_title($this->__('News'));

        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('news_id');
       
        $model = Mage::getModel('news/news');//

        // 2. Initial checking
        if ($id) {
          
           $model->load($id);
            if (! $model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('news')->__('This news no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }
        
        $this->_title($model->getId() ? $model->getTitle() : $this->__('New News'));

        // 3. Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $model->setData($data);
        }

        // 4. Register model to use later in blocks
        Mage::register('news', $model);

        // 5. Build edit form
        $this->_initAction()
            ->_addBreadcrumb(
                $id ? Mage::helper('news')->__('Edit News')
                    : Mage::helper('news')->__('New News'),
                $id ? Mage::helper('news')->__('Edit News')
                    : Mage::helper('news')->__('New News'));

        $this->renderLayout();
    }

   
    
    /**
     * Save action
     */
    public function saveAction()
    {   
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {
            $data = $this->_filterPostData($data);
            //init model and set data
            $model = Mage::getModel('news/news');

            if ($id = $this->getRequest()->getParam('news_id')) {
                $model->load($id);
            }

           
            //for the resize image
            $helper = Mage::helper('news/image');
            //formed by the full path to the file
            $helper->init($model, 'path_img')
                   ->keepAspectRatio(true)
                   ->resize(200);
            
            $model->setData($data);
            //Handles file upload on admin pages
            $model->handleUploads();
            Mage::dispatchEvent('news_prepare_save', array('news' => $model, 'request' => $this->getRequest()));
            //validating
            if (!$this->_validatePostData($data)) {
                $this->_redirect('*/*/edit', array('news_id' => $model->getId(), '_current' => true));
                return;
            }

            // try to save it
            try {
                // save the data
                $model->save();

                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('news')->__('The news has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('news_id' => $model->getId(), '_current'=>true));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
            catch (Exception $e) {
                $this->_getSession()->addException($e,
                    Mage::helper('news')->__('An error occurred while saving the new.'));
            }

            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', array('news_id' => $this->getRequest()->getParam('news_id')));
            return;
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('news_id')) {
            $title = "";
            try {
                // init model and delete
                $model = Mage::getModel('news/news');
                $model->load($id);
                $title = $model->getTitle();
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('news')->__('The news has been deleted.'));
                // go to grid
                Mage::dispatchEvent('adminhtml_news_on_delete', array('title' => $title, 'status' => 'success'));
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::dispatchEvent('adminhtml_news_on_delete', array('title' => $title, 'status' => 'fail'));
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('news_id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('news')->__('Unable to find a new to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName()) {
            case 'new':
            case 'save':
                return Mage::getSingleton('admin/session')->isAllowed('news/save');
                break;
            case 'delete':
                return Mage::getSingleton('admin/session')->isAllowed('news/delete');
                break;
            default:
                return Mage::getSingleton('admin/session')->isAllowed('news/page');
                break;
        }
    }

    /**
     * Filtering posted data. Converting localized data if needed
     *
     * @param array
     * @return array
     */
    protected function _filterPostData($data)
    {
        $data = $this->_filterDates($data, array('custom_theme_from', 'custom_theme_to'));
        return $data;
    }

    /**
     * Validate post data
     *
     * @param array $data
     * @return bool     Return FALSE if someone item is invalid
     */
    protected function _validatePostData($data)
    {
        $errorNo = true;
        if (!empty($data['layout_update_xml']) || !empty($data['custom_layout_update_xml'])) {
            /** @var $validatorCustomLayout Mage_Adminhtml_Model_LayoutUpdate_Validator */
            $validatorCustomLayout = Mage::getModel('adminhtml/layoutUpdate_validator');
            if (!empty($data['layout_update_xml']) && !$validatorCustomLayout->isValid($data['layout_update_xml'])) {
                $errorNo = false;
            }
            if (!empty($data['custom_layout_update_xml'])
            && !$validatorCustomLayout->isValid($data['custom_layout_update_xml'])) {
                $errorNo = false;
            }
            foreach ($validatorCustomLayout->getMessages() as $message) {
                $this->_getSession()->addError($message);
            }
        }
        return $errorNo;
    }
    
}