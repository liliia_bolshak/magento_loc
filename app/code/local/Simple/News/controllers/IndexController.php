<?php



/**
 * News index controller
 *
 * @category   Simple
 * @package    Simple_News
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Simple_News_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     *  News list action
     */
    public function listAction()
    {
        
        $this->loadLayout();
        $this->renderLayout();
        
    }
    
    /**
     *  News views action
     */
    public function viewAction()
    {
        if ($this->_initNews()) {
            $this->loadLayout();
            $this->renderLayout();
        } else {
            $this->_forward('noRoute');
        }
    }
  
    /**
     * Inits news object to work with
     * check Referer and check for membership to store
     */
    protected function _initNews()
    {
        $id = $this->getRequest()->getParam('news_id');
        $referer = $this->getRequest()->getHeader('referer');
        $news = Mage::getModel('news/news')->load($id);

        if ($this->_validateNews($news) || strpos($referer,'admin/news') !== false ) {
            // Register model to use later in blocks
            Mage::register('news', $news);
            return $news;
        } else {
            return false;
        }
    }
    /**
     * check the model
     */
    protected function _validateNews($news)      
    {
        $stores = array(Mage::app()->getStore()->getStoreId(), 0);

        if (!$news->getId() || 
            !array_intersect($stores, (array)$news->getStores())||
            !$news->getIsActive()) {

            return false;
        } else {
            return true;
        }
    }
}
