<?php

class Simple_News_Model_News extends Mage_Core_Model_Abstract
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    const THUMB = 'path_img';
    
    protected function _construct()
    {
        $this->_init('news/news');
    }
    
     public function getAvailableStatuses()
    {
        $statuses = new Varien_Object(array(
            self::STATUS_ENABLED => Mage::helper('news')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('news')->__('Disabled'),
        ));

       

        return $statuses->getData();
    }

    /**
     * Handles file upload on admin pages
     */
    public function handleUploads()
    {
        $path = Mage::getBaseDir('media') . DS . 'news' . DS ;
        //handling thumb image upload
        $pathImage = $this->getPathImg();

        if (!empty($_FILES[self::THUMB]['name'])) {
            try {
                $uploader = new Varien_File_Uploader(self::THUMB);
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png')); // or pdf or anything
                /**
                 * If _allowRenameFiles variable is set to TRUE, uploaded file name will be changed if some
                 *file with the same name already exists in the destination directory (if enabled). 
                */ 
                $uploader->setAllowRenameFiles(true);
                //If this variable is set to TRUE, files despersion will be supported
                $uploader->setFilesDispersion(true);
                $results = $uploader->save($path, $_FILES[self::THUMB]['name']);
                /**
                 * If you choose to delete checkbox, the file must be physically removed
                 */
                //deleting previous file
                if (is_array($pathImage)) {
                    unlink($path . $pathImage['value']);
                } elseif (!empty($pathImage)) {
                    unlink($path . $pathImage);
                }

                //setting path to new uploade image 
                $this->setData(self::THUMB, $results['file']);
            } catch (Exception $e) {
                
            }
        } elseif (!empty($pathImage['delete']) && (!empty($pathImage['value']))) {
            unlink($path . $pathImage['value']);
            $this->setData(self::THUMB, '');
        } elseif (!empty($pathImage['value'])) {
            $this->setData(self::THUMB, $pathImage['value']);
        }
    }

}
