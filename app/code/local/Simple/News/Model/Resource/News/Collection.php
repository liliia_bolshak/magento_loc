<?php
class Simple_News_Model_Resource_News_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Define resource model
     *
     */
    protected function _construct()
    {
        $this->_init('news/news');
    }
    /**
     * Prepares a filter for the store
     *
     */

    public function addStoreFilter($store, $withAdmin = true)
    {
        if ($store instanceof Mage_Core_Model_Store) {
            $store = array($store->getId());
        }

        if (!is_array($store)) {
            $store = array($store);
        }

        if ($withAdmin) {
            $store[] = Mage_Core_Model_App::ADMIN_STORE_ID;
        }
//Add collection filter
        $this->addFilter('store_id', array('in' => $store), 'public');

        return $this;
    }

    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSubSelect = new Zend_Db_Expr('(' . $countSelect . ')');
       //Clear parts of the Select object, or an individual part
        $countSelect->reset()
            ->from($countSubSelect)
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns('COUNT(*)');

        return $countSelect;
    }

//Join store relation table if there is store filter
    protected function _renderFiltersBefore()
    {
        if ($this->getFilter('store_id')) {
            $this->getSelect()->join(
                array('store_table' => $this->getTable('news/news_store')),//Retnamerieve table name
                'main_table.news_id = store_table.news_id',
                array()
            )->group('main_table.news_id');

            /*
             * Allow analytic functions usage because of one field grouping
             */
            $this->_useAnalyticFunction = true;
        }
        return parent::_renderFiltersBefore();
    }

    
}
