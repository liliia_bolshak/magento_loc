<?php
class Simple_News_Model_Resource_News extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('news/news', 'news_id');
    }

    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        
        $insert = (array)$object->getStores();
        $table  = $this->getTable('news/news_store');

        $where = array(
            'news_id = ?'     => (int) $object->getId(),
        );
        $this->_getWriteAdapter()->delete($table, $where);

        if ($insert) {
            $data = array();

            foreach ($insert as $storeId) {
                $data[] = array(
                    'news_id'  => (int) $object->getId(),
                    'store_id' => (int) $storeId
                );
            }

            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }

        return parent::_afterSave($object);

    }

    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());
            $object->setData('store_id', $stores);
            $object->setData('stores', $stores);
        }

        return parent::_afterLoad($object);
    }
    /**
     * 
     * Get store ids to which specified item is assigned
     */
    public function lookupStoreIds($id)
    {
        $adapter = $this->_getReadAdapter();

        $select  = $adapter->select()
            ->from($this->getTable('news/news_store'), 'store_id')
            ->where('news_id = :news_id');

        $binds = array(
            ':news_id' => (int) $id
        );

        return $adapter->fetchCol($select, $binds);
    }
}
