
function popupWindow(url) {

    /*if ($('popup_window') &amp;&amp; typeof(Windows) != 'undefined') {
        Windows.focus('popup_window');
        return;
    }*/

    oPopup = new Window({
        closable:true,
        id:'popup_window',
        className: 'magento',
        width: 1080,
        height: 750,
        url: url,
        resizable: true,
        minimizable: false,
        maximizable: false,
        showEffectOptions: {
            duration: 0.4
        },
        hideEffectOptions:{
            duration: 0.4
        },
        destroyOnClose: true,
        closeCallback: function() { productGridJsObject.reload(); return true; }

    });

    oPopup.setZIndex(1000);
    oPopup.showCenter(true);
    }

document.observe("dom:loaded", function() {
    if (top !== window && top.productGridJsObject !== "undefined" &&
        window.location.toString().match('save')) {
        setTimeout(function () {
            top.oPopup.close();
        }, 2000);
    }
});



